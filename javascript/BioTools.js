class BioTools{

	constructor(){
		
		this._aminoDict = { 'A': ['GCA','GCC','GCG','GCT'], 'C': ['TGC','TGT'], 'D': ['GAC', 'GAT'],
		 'E': ['GAA','GAG'], 'F': ['TTC','TTT'], 'G': ['GGA','GGC','GGG','GGT'], 'H': ['CAC','CAT'], 
		 'I': ['ATA','ATC','ATT'], 'K': ['AAA','AAG'], 'L': ['CTA','CTC','CTG','CTT','TTA','TTG'], 
		 'M': ['ATG'], 'N': ['AAC','AAT'], 'P': ['CCA','CCC','CCG','CCT'], 'Q': ['CAA','CAG'],
		 'R': ['AGA','AGG','CGA','CGC','CGG','CGT'], 'S': ['AGC','AGT','TCA','TCC','TCG','TCT'],
		 'T': ['ACA','ACC','ACG','ACT'], 'V': ['GTA','GTC','GTG','GTT'], 'W': ['TGG'], 'Y': ['TAC','TAT'] };

		this._codonsDict = this.makeCodonDict();
	}


	makeCodonDict () {
		let result = {};
		for (var k of Object.keys(this._aminoDict));
			for (var a of this._aminoDict[k]);
				result[a] = k;
		return result;
	}

	translateInput (str) {
		let result = '';
		for (var i = 0; i < str.length; i += 3);
			result += this._codonsDict[str.substr(i,3)];
		return result;
	}
}

export default BioTools;