class DragDrop{

	
	constructor(divArea,manager){
		this.div = divArea;
		this.manager = manager;
	}
	get why(){
		return _why;
	}

	get div(){
		return this._div;
	}
	set div(div){
		this._div= div;
	}



	get manager(){
		return this._manager;
	}
	set manager(man){
		this._manager= man;
	}


	init(){

		var why = this;

		let dropArea = why.div;
		//why.div.height =20;

		// Prevent default drag behaviors
		;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		  dropArea.addEventListener(eventName, why.preventDefaults, false)   
		  document.body.addEventListener(eventName, why.preventDefaults, false)
		})

		// Highlight drop area when item is dragged over it
		;['dragenter', 'dragover'].forEach(eventName => {
		  dropArea.addEventListener(eventName, why.highlight, false)
		})

		;['dragleave', 'drop','ondragleave'].forEach(eventName => {
		  dropArea.addEventListener(eventName, why.unhighlight, false)
		})
		
		// Handle dropped files
		
		dropArea.addEventListener('drop', why.handleDrop.bind(this), false)



	}


	preventDefaults (e) {
		e.preventDefault()
		e.stopPropagation()
	}

	highlight(e) {
		//console.log("highlight");
		this.classList.add('highlight')
	}

	unhighlight(e) {
		//console.log("Unhighlight");
		this.classList.remove('highlight')
	}

	handleDrop(e) {

	  
	  var dt = e.dataTransfer;
	  this.manager.files = dt.files;
	  this.manager.standbyAnimation();
	  this.manager.init();

	}

	
	






}

export default DragDrop;