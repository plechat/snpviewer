import GenomicObject from "./GenomicObject";

class Snp extends GenomicObject{
	
	constructor(id,begin,end,direction,gene,freq,coverage,reference,alternate,colorSNP,aa,aamut){
		super(begin,end,direction);
		this._id=id;
		this._gene =gene;
		this._freq =freq;
		this._coverage=coverage;
		this._reference =reference;
		this._alternate = alternate;
		this._color = colorSNP;
		this._aa =aa;
		this._aamut = aamut;

		//this._fromAbs=begin;
		//this._toAbs =end;
		//this._direction = direction;
	}
	get length(){
		return 1;
	}
	get freq(){
		return this._freq;
	}
	get alternate(){
		return this._alternate;
	}
	get reference(){
		return this._reference;
	}
	get coverage(){
		return this._coverage;
	}
	get color(){
		return this._color;
	}
	get aa(){
		return this._aa;
	}
	get aamut(){
		return this._aamut;
	}
	get gene(){
		if (this._gene == "undefined"){
			this._gene = "";	
		}
		return this._gene;

	}

	get synonymous(){
		if (this.aa == this.aamut){
			return true;
		}
		else{
			return false;
		}
	}


}


export default Snp;
