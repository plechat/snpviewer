import $ from 'jquery';
import Konva from 'konva';
import ColorGradient from "./ColorGradient";

class InterfaceElement {
    constructor(stageRef, stageSnp,stageN) {


        
        let dropAreadiv;


        this._canvas;
        this._canvasSnp;
        this._stageSnpMap =stageSnp;
        this._stageRefMap =stageRef;

        this._layerSnp;
        this._layerRef;
        this._layerNames;

        this._stageNames=stageN;

        this._tooltipLayer;

        this._manager;

        // zoom in position x genomic position of the mouse in zoom start
        this.xGenomic  =0;

    }
    set manager(man){
		this._manager=man;
	}

    get manager(){
        return this._manager;
    }

    get stageRefMap(){
		return this._stageRefMap;
    }    

    get layerRef(){
		return this._layerRef;
    }
    get layerSnp(){
		return this._layerSnp;
    }
    get tooltipLayer(){
		return this._tooltipLayer;
    }
    set tooltipLayer(tooltl){
		this._tooltipLayer = tooltl;
	}

    get canvas(){
		return this._canvas;
    }
    get canvasSnp(){
		return this._canvasSnp;
    }


    createCanvas(){

        console.log("createCanvas");
        console.log(this._stageRefMap);
        /*this._stageRefMap = new Konva.Stage({
            container: 'refMap',
            width: $(refMap).width,
            height: 50
                    
            });*/


        console.log("refmap "+this._stageRefMap);
        console.log("refmap "+ $(refMap).width());

        console.log(document);

        this._canvas = document.createElement("canvas");
        this._canvas.width = $(refMap).width();
        console.log("canvaswidth "+this._canvas.width);
        console.log(this._canvas);

        var imageRef = new Konva.Image({
            image: this._canvas,
            x: 0,
            y: 0
        
            });
        this._layerRef = new Konva.Layer();
        this._layerRef.add(imageRef);
        

       

        

        /*console.log(this._stageRefMap);
        var circle = new Konva.Circle({
            x: 20,
            y: 20,
            radius: 70,
            fill: 'red',
            stroke: 'black',
            strokeWidth: 2
        });
        // add the shape to the layer
        this._layerRef.add(circle);*/


        // add the layer to the stage
        this._stageRefMap.add(this._layerRef);

        /*this.stageSnpMap = new Konva.Stage({
            container: 'subMap',
            width: $(refMap).width(),
            height: 1000,
            
            });*/
        this._layerSnp = new Konva.Layer();
        this._tooltipLayer = new Konva.Layer();
        console.log(this._tooltipLayer);
            


        this._canvasSnp = document.createElement("canvas");
        this._canvasSnp.width = $(refMap).width();
        this._canvasSnp.height =3000;
            //document.getElementById("subMap").appendChild(canvasSnp);
        console.log(this._canvasSnp);

        var image = new Konva.Image({
            image: this._canvasSnp,
            x: 0,
            y: 0
            
        });
        this._layerSnp.add(image);
        this._stageSnpMap.add(this._layerSnp);
        
       


        this._stageSnpMap.add(this._tooltipLayer);

            
        /*this.stageNames = new Konva.Stage({
            container: 'namesC',
            width: $(refMap).width(),
            height: 100,
                
        });
        */
        this._layerNames = new Konva.Layer();
        

        this._stageNames.add(this._layerNames);

    }

    initElements(){

        // Slider Frequency
        $( "#slider-range" ).slider({
              width:300,
              height:50,
              range: true,
              min: 0,
              step: 0.5,
              max: 100,
              values: [ 0, 100 ],
              slide: function( event, ui ) {
                $( "#amount" ).val( "%" + ui.values[ 0 ]/100 + " - %" + ui.values[ 1 ]/100 );
                this.manager.setMapsFreq(ui.values[ 0 ] /100,ui.values[ 1 ] /100);
                this.doRedraw();
              }.bind(this)
        });
        $( "#amount" ).val( "%" + $( "#slider-range" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-range" ).slider( "values", 1 ) );

        // Slider coverage
		$( "#slider-coverage" ).slider({
		  width:300,
		  height:50,
		  min: 0,
		  max: 5000,
		  
		
		  slide: function( event, ui ) {
			$( "#coverageRange" ).val(  $( "#slider-coverage" ).slider( "value" ));
			this.manager.setMapsCoverage(ui.value);
			this.doRedraw();
		  }.bind(this)
		});
		$( "#coverageRange" ).val(  $( "#slider-coverage" ).slider( "value" ));
		// Slider Heatmap 
		$( "#slider-heatmap" ).slider({
            width:300,
            height:50,
            range: true,
            min: 1,
            values: [ 1, 1000 ],	  
            slide: function( event, ui ) {
                $( "#heatmapRange" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ]);
                this.manager.setMapsHeatMapRange(ui.values[ 0 ] ,ui.values[ 1 ] );
    
                this.doRedraw();
            }.bind(this)
		});
		$( "#heatmapRange" ).val( "%" + $( "#slider-heatmap" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-heatmap" ).slider( "values", 1 ) );
        // Slider Range 
        
		
		/*$( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );*/
	
        // color Option
        $( "#colorSnpFrequence" ).click(function() {
            console.log(this);
            this.doDraw();
        }.bind(this));
        $( "#colorSnpHeatmap" ).click(function() {
            this.doDraw();
        }.bind(this));
        $( "#colorSnpBasecolor" ).click(function() {
            this.doDraw();
        }.bind(this));
        $( "#colorSnpSyn" ).click(function() {
            this.doDraw();
        }.bind(this));
        
        $('#refMap').on('mousewheel', function (e) {
            this.mouseWeelHandler(e);
            //prevent page fom scrolling
            return false;
        }.bind(this));
        // event on submap and refmap
        
        $('#subMap').on('mousewheel', function (e) {
            this.mouseWeelHandler(e);
            
        }.bind(this));


    }
    
    setMapLength(){

        $( "#widget" ).dblclick(this.dblclickhandler.bind(this));

        //console.log($( "#slider-position" ));
        console.log(this.manager.mapLength);
        //$("#slider-position").slider('values',1,this.manager.mapLength); 
        $( "#slider-position" ).slider({
            width:300,
            height:50,
            range: true,
            min: 0,
            max: this.manager.mapLength,
            values: [ 0, this.manager.mapLength ],
            slide: function( event, ui ) {
                $( "#positionSlider" ).val( "bp" + ui.values[ 0 ] + " - bp" + ui.values[ 1 ]);
                this.manager.setMapsPosition(ui.values[ 0 ] ,ui.values[ 1 ] );
    
                this.doRedraw();
            }.bind(this)
		}).bind(this);
        $( "#positionSlider" ).val( "bp" + 1 + " - bp" + this.manager.mapLength);




    }

    dblclickhandler(){
		console.log("doubleclick");
       // let why = this;
		this.xGenomic = 0;
       
        this.doDraw();
        
        let mapLength =this.manager.mapLength;
        console.log(this.manager.mapLength)

		//nZoom = 1;
		$("#slider-position").slider("values" , [0,this.manager.mapLength]).bind(this);
		/*$( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );*/

		this.goTOGenomicPosition(1,mapLength );
		
	}


    removeDropArea(){
        let widg = document.getElementById("widget");
        let dropA = document.getElementById("drop-area");

        //let widg2 = $("widget");
        //let dropA =  $("drop-area");

        widg.removeChild(dropA);
    }

    initSlider(){

        $( "#slider-heatmap" ).slider("option", "max", this.manager._strainsNumber);
        $( "#heatmapRange" ).val( "%" + $( "#slider-heatmap" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-heatmap" ).slider( "values", 1 ) );
        
        console.log("initSlider");


    }

    doDraw(){

        //console.log("doDraw !!!!");
        //manager.mapRef.createScale(1,$( "#refMap" ).height());

        // to Move colorGradient 

        //console.log(this.manager._strainsNumber);
		var colorG = new ColorGradient(this.manager._strainsNumber,"#4ad4ff","#FF0000",1); 
        //#FCFAE1 #BD8D46
		//#63FFB2 #4A2612
		var colors = colorG.updateSpitter();
        //console.log("!!!!!!!"+colors.length);  
    	//for (var i=0;i<colors.length;i++){
        //    console.log(colors[i]);
        //}
        
        //console.log("ColorGradient "+colorG.val1El);
        this.manager.colorHeatmap = colors;

        // compute mapRef and draw
        this._manager.mapRef.computeMap();
        //console.log("layerRef "+this._layerRef);
        this._layerRef.draw();

        var tabStrains;
        //console.log(this._manager.strainManager);
        /*if (this._manager.strainManager === undefined){

            console.log("IIIIIIIII");
            tabStrains = this._manager.mapsSnp;
        }
        else{
            console.log("AAAAAAAAAAA");
            tabStrains = this._manager.strainManager.getStrainsByType("tp");
            console.log(tabStrains);
        }*/
        tabStrains = this._manager.mapsSnp;

        //tabStrains = this._manager.mapsSnp;
        // compute mapSNPs and draw
        //for (var variable in this._manager.mapsSnp) {
        for (var variable in tabStrains) {
            
           // console.log("variable "+variable);
            
            let isFreq =false;
            let isHeatmap =false;
            let isSyn =true;

            if($( "#colorSnpFrequence:checked" ).val() =='frequence'){
                isFreq =true;
              }
            else{
                isFreq= false;
    
            }
            if($( "#colorSnpHeatmap:checked" ).val() =='heatmap'){
                isHeatmap =true;
            }
            else{
                isHeatmap= false;
    
            }
            if($( "#colorSnpSyn:checked" ).val() =='syn'){
                isSyn =true;
            }
            else{
                isSyn= false;
    
            }

            this._manager.mapsSnp[variable].computeMapCanvas(isFreq,isHeatmap,isSyn);
        }

        
        //this._layerSnp.draw();
        this._layerSnp.batchDraw();
        //this._layerNames.draw();

    }

    // deal with the files in a Server
    serverAccess(){

        var server="http://localhost/devPoo/data/importOct2K18/newcarib/";
        var reference="carib.tsv";
        var snpsdir ="snps/";

        var loader = new LoadData(server,reference,snpsdir);
        console.log("s: "+server);

        loader.loadReference().then(
            loader.loadSnps().then(() => console.log('Finished'))
        );

        //var pttLoader = new PttLoader(server+"carib/Chik_carib.fasta");
        //pttLoader.read();
        //console.log("ICI");
        
        //loader.loadSnps().then(() => console.log('Finished'));

    }




    // A INTEGRER !!!!!!!!!!!!!!

    goTOGenomicPosition(xG,yG){
        console.log("goTOGenomicPosition "+xG+" "+yG );
    
        this.xGenomic =xG;
        this._manager.setMapsPosition(xG,yG );
        
        //$( "#positionSlider" ).val( "bp" + xG + " - bp" + yG);
       
        // $("#slider-position").slider("values" , [xG,yG]);
       // $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
        //    " - " + $( "#slider-position" ).slider( "values", 1 ) );
        this.doRedraw();
    }


    doRedraw(){
        this.doErase();
        this.doDraw();

    }

    doErase(){
            
        //manager.setMapsRange(5000,12000);
        this._manager.mapRef.erase();

        for (var variable in this._manager.mapsSnp) {
            this._manager.mapsSnp[variable].erase();
        }

        var rects = this._layerSnp.getChildren(function(node){
            return node.getClassName() === 'Text';

        });
        for (var i=0; i<rects.length; i++) {
            //console.log(rects[i]);
            rects[i].destroy();
        }
        var textsRef = this._layerRef.getChildren(function(node){
            return node.getClassName() === 'Text';

        });
        for (var i=0; i<textsRef.length; i++) {
            //console.log(rects[i]);
            textsRef[i].destroy();
        }
        var rectsRef = this._layerRef.getChildren(function(node){
            return node.getClassName() === 'Rect';

        });
        for (var i=0; i<rectsRef.length; i++) {
            //console.log(rects[i]);
            rectsRef[i].destroy();
        }
        // Mutation VOC variant of concern
        var trianglRef = this._layerRef.getChildren(function(node){
            return node.getClassName() === 'RegularPolygon';

        });
        for (var i=0; i<trianglRef.length; i++) {
            //console.log(rects[i]);
            trianglRef[i].destroy();
        }


        // LayerNames
        //this._layerNames.erase();
        /*var textsNames = this._layerNames.getChildren(function(node){
            return node.getClassName() === 'Text';

        });
        for (var i=0; i<textsNames.length; i++) {
            //console.log(rects[i]);
            textsNames.destroy();
        }


        this._layerNames.draw();*/


        this._layerRef.draw();
        this._layerSnp.draw();
        

    }


    mouseWeelHandler(e){
		e.preventDefault();
		//console.log("! "+manager.mapRef);
		//console.log("!! "+manager.mapRef.fin);

		if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) { //alternative options 
			var x = e.pageX - $('#refMap').offset().left;
			var fintmp = this.manager.mapRef.fin - this.manager.mapRef.debut+1;
			this.xGenomic = (x * fintmp) /this.manager.mapWidth+this.manager.mapRef.debut;
			this.zout(e);
		} else {

			
			var x = e.pageX - $('#refMap').offset().left;
			var fintmp = this.manager.mapRef.fin - this.manager.mapRef.debut+1;
			this.xGenomic = (x * fintmp) /this.manager.mapWidth+this.manager.mapRef.debut;
		
			this.zin(e);
		}
		//prevent page fom scrolling
		return false;
	}

    zout(e){
		
		var newX;
		var newY;
		  newX = this.xGenomic -(this.xGenomic-this.manager.mapRef.debut)*(1.2);       
		  newY = (this.manager.mapRef.fin-this.xGenomic)*(1.2) + this.xGenomic;
		  
		  $("#slider-position").slider("values" , [newX,newY]);
		  $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );
		  if((newY-newX)< this.manager.mapLength){
			this.manager.setMapsPosition(newX,newY );
			this.doRedraw();
		  }
  
	}
	zin(e){
		
		var newX;
		var newY;
	
		  
		newX = this.xGenomic -(this.xGenomic - this.manager.mapRef.debut)*0.8;
		newY = this.xGenomic +(this.manager.mapRef.fin-this.xGenomic)*0.8;  
		
		$("#slider-position").slider("values" , [newX,newY]);
		  $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );
		if(newY-newX>25){
            this.manager.setMapsPosition(newX,newY);
		  this.doRedraw();
		}
		
	}


    

}	
    
export default InterfaceElement;