class LoadData{
	constructor(server,filePivot,snpsdir){
		this._server =server;
		this._filePivot =filePivot;
		this._snpsDir = snpsdir;


	}

	get server(){
		return this._server;
	}

	get filePivot(){
		return this._filePivot;
	}

	get snpsdir(){
		return this._snpsDir;
	}


	load(){

	}

	async loadReference(){
		var pttLoader = new PttLoader();
		var url = this.server+this.filePivot;
		console.log(url);
		var promess = pttLoader.load(url);
		var resp = await promess;
		//console.log(resp);
		//console.log(resp.data);
		//console.log(file);
		pttLoader.parsePttFile();
		//console.log(resp);
	}


	async loadSnps(){

		var files = ['p1_PHH3_B5_S11.snp','p1_PHH3_H4_S10.snp','p1_PHH3_W4_S9.snp','p1_PHH3_d7_H1_ND_S17.snp'];
	    var url = this.server+this.snpsdir;

	    var promises = new Array();
		for (var i=0;i<files.length;i++){
			var fileName = files[i];
			var snpLoader = new SnpLoader();
	    	console.log(snpLoader);
	    	var file = url+fileName;
	    	//snpLoader.load(file);
	    	var p  = snpLoader.load(file);
	    	//snpLoader._data = p;
	        promises.push(p);
		}    
	    for (var i=0;i<promises.length;i++){
			
			var promesse = promises[i];
			var resp = await promesse;
			
			var org = resp.parseSNPfile();
			//console.log(org.name);
			//console.log(resp);   
		}

	}

}

export default LoadData;