import GenomicMap from "./GenomicMap";

var jquery = require("jquery");
window.$ = window.jQuery = jquery;
var Tipped = require("@staaky/tipped");
import '../tipped.css';


class SnpMap extends GenomicMap{

	constructor(canvas,mapL,mapH,mapW,repliconL,title,manager,cpt){
		super(canvas,mapL,mapH,mapW,repliconL,title,manager);
		
		this._snps =[];

		this.freqMin=0;
		this.freqMax=1;
		this.coverMin=0;

		this.heatMapMin=0;
		this.heatMapMax=1000;
		this._indice = cpt;
		this.offsetY = cpt*10;
		
		this.tabSnpKonva; //i -> snp tooltip : konva Name -> snp !!!
	        
		/*snpstab.forEach(function(d) { 
	             //console.log(d.from+" "+d.nucW);
	            //console.log(d.from+" "+d.nucW+" "+d.gene);
	            //console.log("!!! "+ svg);
	            //(id,begin,end,direction,gene){
	           	var snp = new Snp('',d.from,'',d.gene); //id,begin,end,direction,gene
	           	this.snps.push(snp);
	        
	    });*/
	    //console.log("!!! _snps.length "+_snps.length);
		//this._snps =snpstab;

	}

	regionSnps(posfrom,posto){
		
		//console.log("regionSnps"+posfrom+" "+posto);
		//var snpsR =_snpD[id_ref];		
		var snps =[];
		
				
		//console.log(posfrom+" "+posto+" "+this.genes.length);
		//console.log("_snps.length"+this._snps.length);
		for (var i=0;i<this._snps.length;i++){
			var snp = this._snps[i] ;
			//console.log("SNP " +snp);	
			//console.log(snp.fromAbs+" "+snp.toAbs);
			if ((snp.fromAbs >= posfrom) && (snp.fromAbs <=posto)){
				//trace(gene.accNum);
				//console.log(gene.accNum);
				snps.push(snp);	
			}
		}

		//console.log("snps.length "+snps.length);
		

		return snps;
		
	}
	erase(){
		//console.log("erase");
		/*while (this.parentSVG.lastChild) {
    		this.parentSVG.removeChild(this.parentSVG.lastChild);
		}*/
		//Methode Canvas
		this.parentCanvas.getContext("2d").clearRect(0, 0, this.parentCanvas.width, this.parentCanvas.height);
		// Methode Layer
		//this.parentCanvas.clear();
		//
	}
	createScale(isgraduate,height){
		
		   	
		var context = this.parentCanvas.getContext('2d');
		context.lineWidth="1";
		var inter = this.mapLength/10;
		context.fillStyle='#eceff4';

		//console.log("inter "+inter+" "+this.mapLength);
		//console.log( "createScale "+this.mapHeight);
		for (var i=0;i<10;i++){
			if (i/2 -Math.round(i/2) ==0){
				//context.fillRect(this.convert(i*inter),0,this.convert(inter),this.mapHeight);
				context.fillRect(this.convert(i*inter),this.offsetY,this.convert(inter),height);		    		
			}

		}
		//document.body.appendChild(svg);
	}
	computeMapSVG(){

		//console.log("computeMap");
		//this.createScale(1);
		//console.log("deb "+this.debut+" "+this.fin);
		var snps = this.regionSnps(this.debut,this.fin);
		//console.log("l " + snps.length);
		//var snps = this._snps;
		//console.log("!-!-!-!-!"+_snps);
		//console.log("!-!-!-!-!"+this.snps);
		//console.log("SNPS Length "+snps.length+" "+this.debut+" "+this.fin);
		var colorSnp;
		//console.log(this.parentSVG);
		//console.log("indice "+this._indice);
		for (var i=0;i<snps.length;i++){
			var snpi = snps[i];
			if (snpi.gene != undefined ){
		                
	            if (snpi.codW == snpi.codM){
	                colorSnp = "#ff69b4"; 
	            }
	            else{
	                colorSnp = "#0000ff";   
	            }
	        }
	        else{
	           // console.log(d.from+" "+d.nucW+" "+d.gene);
	            colorSnp ="#ff69b4";
	            if (snpi.freq < 0.1){
	            //if ((snpi.freq > this.freqMin)&&(snpi.freq < this.freqMax)){
	            	colorSnp ="#0000FF";
	            }
	            else{
	            	colorSnp ="#FF0000";
	            }   
	        }
	        //colorSnp ="#ff69b4";
	        var xmlns = "http://www.w3.org/2000/svg";
			var rect = document.createElementNS(xmlns, "rect");
	        //var rect = document.createElementNS(svgNS,'rect');
	        rect.setAttribute("fill",colorSnp);
	        
	        //console.log(this.convert(snpi.fromAbs));
	        rect.setAttribute('x',this.convert(snpi.fromAbs));
	        //rect.setAttribute('y',0);
	        rect.setAttribute('y',this.indice*10);
			rect.setAttribute('width',1);
			rect.setAttribute('height',10);
			

			//rect.setAttribute('class',"ui-selectee");
			///rect.setAttribute('fill',colorSnp);
			//console.log("appendRect "+rect);
			if ((snpi.freq > this.freqMin)&&(snpi.freq < this.freqMax)){
	        	this.parentSVG.appendChild(rect);
	    	}
        }

	}
	computeMapCanvas(isFreq,isHeatmap,isSyn){

		// Add the Names to LayerNames

		//console.log("computeMapCanvas" + this.cpt+" "+ this.title);
		//this.createScale(1,this.mapHeight);
		this.tabSnpKonva =new Array();
		if (this.manager._treeLoader ==null){
			var simpleText = new Konva.Text({
				x: 0,
				y: this._indice*10,
				text: this.title,
				fontSize: 8,
				fill: 'black'
			});
			this.manager.parent._layerNames.add(simpleText);
		}
		
		this.tabSnpKonva =new Array();

		//console.log("computeMapCanvas");
		
		//console.log("deb "+this.debut+" "+this.fin);
		var snps = this.regionSnps(this.debut,this.fin);
		
			
		//console.log("l " + snps.length);
		//var snps = this._snps;
		//console.log("!-!-!-!-!"+_snps);
		//console.log("!-!-!-!-!"+this.snps);
		//console.log("SNPS Length "+snps.length+" "+this.debut+" "+this.fin);
		var colorSnp;
		//console.log(this.parentSVG);
		//console.log(this.parentCanvas);
		var context = this.parentCanvas.getContext('2d');
		//console.log("w "+this.parentCanvas.width);
		context.shadowBlur = 0;

		//console.log("conv!!!! " + this.conv);
		let strainName = this.title;
		var dictSnpHeatmap = this.manager.heatmapSnpDict;
		var colors = this.manager.colorHeatmap;


		//console.log("indice "+this._indice);

		var y;
		if (this.manager._treeLoader ==null){

			y =this.offsetY;
		}
		else{
			var treeLoader = this.manager._treeLoader;
			y = treeLoader.computeLeafY(this.title);
		}

		// Map Separation line below reference map
		context.fillStyle= 'black';
		context.globalAlpha = 0.3;
		context.beginPath();
		
		context.moveTo(0,y);
		context.lineTo(this.mapWidth, y);
		context.stroke();
		context.globalAlpha = 1;


		for (var i=0;i<snps.length;i++){
			var snpi = snps[i];
			/*if (snpi.gene != undefined ){
		                
	            if (snpi.codW == snpi.codM){
	                colorSnp = "#ff69b4"; 
	            }
	            else{
	                colorSnp = "#0000ff";   
	            }
	        }
	        else{*/
	        var heatKey = snpi.fromAbs+snpi.alternate;
	        var heatValue = dictSnpHeatmap[heatKey];
	        if (isFreq){
	           // console.log(d.from+" "+d.nucW+" "+d.gene);
	            //colorSnp ="#ff69b4";
	            if (snpi.freq < 0.1){
	            //if ((snpi.freq > this.freqMin)&&(snpi.freq < this.freqMax)){
	            	colorSnp ="#0000FF";
	            }
				else if ((snpi.freq > 0.1)&&(snpi.freq < 0.3)){
					colorSnp ="#00FF00";
				}
	            else{
	            	colorSnp ="#FF0000";
	            }   
	        
	        }
	        else if (isHeatmap){
	        	//colorSnp ="#FFFF00";	        	
	        	colorSnp = colors[heatValue];
	        }
	        else if (isSyn){
	        	//colorSnp ="#FFFF00";
	        	if (snpi.gene !=""){	  
		        	if (snpi.synonymous){      	
		        		colorSnp = "#0000FF";
		        	}
		        	else {
		        		colorSnp = "#FF00FF";
		        	}
		        }
		    	else{
		    		colorSnp = "#000000";
		    	}

	        }
	        else{
	        	colorSnp = snpi.color;
	        }

	        var x= this.convert(snpi.fromAbs)-this.convert(this.debut);

	        //var y= this._indice*10;
	        
	        
			//var y =this.offsetY;
			
	        
			//this.parentCanvas.rect(x,y,1,10);
			
			//context.beginPath();
			context.lineWidth="1";
			context.fillStyle=colorSnp;
			//context.strokeStyle="#00FF00";
			//console.log("!!! "+x);
			//context.fillRect(x,0,1,10);
			

			if ((snpi.freq >= this.freqMin)&&(snpi.freq <= this.freqMax)){
	        	//context.rect(x,0,1,10);
	        	//context.fill();
	        	if ((heatValue >= this.heatMapMin)&&(heatValue <= this.heatMapMax)){
					
					if (snpi.coverage >= this.coverMin){

						if(this.fin-this.debut>100){

							context.fillRect(x,y,1.5,5);
							//context.fillRect(x,y,1,5);
						}
						else{

							// draw SNP with tooltip
							// compute codon

							//let biotool = this.manager._biotool;
							
							//const nuc = 'AAG';						
							//const aminos = biotool.translateInput(nuc);
							//console.log(aminos) // KHRNRG
							this.tabSnpKonva[i]=snpi;
					
							let posAA="";
							if(snpi.gene !=""){
								let gene = this.manager._hashNameGenes[snpi.gene];
								//console.log(snpi.gene);
								if(gene!=null){
									posAA = Number.parseInt((snpi.fromAbs -gene.fromAbs+1)/3);
								}
								
							}

							//Ref/pos/mut
							//Nuc T/1359/C
							//Prot I/292/T

							//console.log("ICI "+snpi.gene+" "+gene.id+ " "+snpi.fromAbs+" "+gene.fromAbs+" "+posAA  );

							/*var name = strainName+"\n\n"
							//"gene: "
							+snpi.gene+"\n"+
							//"Ref/pos/mut\n"+        		
							//"Nuc "
							snpi.reference+"/"+snpi.fromAbs+"/"+snpi.alternate+"\n"+
							//"Prot "
							snpi.aa+"/"+posAA+"/"+snpi.aamut+"\n"+
							"Freq: "+snpi.freq+"\n"+
							"Cov: "+snpi.coverage;*/
							
							

							var simpleText = new Konva.Text({
								x: x,
								y: y,
								text: snpi.alternate,
								fontSize: 10,
								name: i+"",
								fill: colorSnp
							});
							/*var rect  = new Konva.Rect({
								x : x, y : y, width: 5, height: 5,
							fill: colorSnp,
							draggable:true
							});*/
							/*var rects = manager.tooltiplayer.getChildren(function(node){
							return node.getClassName() === 'Text';

							});*/
							//console.log(manager.tooltiplayer);
							

							//var tooltipLayer = this.manager.parent._tooltipLayer;
							
							//console.log(this.manager.parent);


							//console.log(tooltipLayer);
							//var tooltip = tooltipLayer.findOne('#face');
							/*var tooltip = new Konva.Text({
								text: "",
								fontFamily: "Calibri",
								fontSize: 12,
								padding: 5,
								textFill: "white",
								fill: "black",
								alpha: 0.75,
								visible: false,
								
							})


							tooltipLayer.add(tooltip);*/

							this.manager.parent._layerSnp.add(simpleText);
							//var stage = tooltipLayer.getStage();

							/*simpleText.on("mousemove", function(e){
								var mousePos = stage.getPointerPosition();
								//var tooltip = tooltipLayer.findOne('#face');
								tooltip.position({
									x : mousePos.x + 5,
									y : mousePos.y + 5
								});
								//tooltip.text(i+" "+strainName+"\n"+snpi.fromAbs+"/"+snpi.alternate+"/"+snpi.reference+":"+snpi.freq);
								tooltip.text(e.target.name());
								tooltip.show();
								tooltipLayer.batchDraw();
							});

							simpleText.on("mouseout", function(){
								//var tooltip = tooltipLayer.findOne('#face');
								tooltip.hide();
								tooltipLayer.draw();
							});*/

							var why = this;
							simpleText.on('mousemove', function(event) {
  
								console.log("mouse enter");
								console.log(event.target);
								console.log(event.target.name);

								var snp = why.tabSnpKonva[parseInt(event.target.name())];
								console.log(snp.reference+"/"+snp.fromAbs+"/"+snp.alternate);
								why.tip('mouse',"snp");
								let gene ="-";
								if(snp.gene !=""){
									gene = why.manager._hashNameGenes[snpi.gene];
									//console.log(snpi.gene);
									if(gene!=null){
										posAA = Number.parseInt((snpi.fromAbs -gene.fromAbs+1)/3);
									}
									gene = snpi.gene;
								}

								let subseq =why.manager._fastaLoader.getSeqAround(parseInt(snp.fromAbs),snp.alternate);
								
								
								$(".strainTip").text(strainName);
								$(".geneTip").text(gene);
								$(".mutTip").text(snp.reference+"/"+snp.fromAbs+"/"+snp.alternate);
								$(".mutATip").text(snp.aa+"/"+posAA+"/"+snp.aamut);
								$(".freqTip").text(snp.freq);
								$(".coverTip").text(snpi.coverage);
								$(".seqTip").text(subseq);

								Tipped.show('#subMap');
								//$( "thayer-tooltip-content" ).html( "<span class='red'>Hello <b>Again</b></span>" );
							
							
							});
							
							simpleText.on('mouseout', function() {
								console.log("mouseleave");
								//Tipped.disable('#container');
								Tipped.hide('#subMap');
								Tipped.disable('#subMap');
								$(".strainTip").text("-");
								$(".geneTip").text("-");
								$(".mutTip").text("-");
								$(".mutATip").text("-");
								$(".freqTip").text("-");
								$(".coverTip").text("-");
							
							});
							
							var why =this;
							simpleText.on( 'click', function(event){
								
								//let clipboard = why.manager._clipboard;
								///console.log(clipboard);
								//clipboard.writeText(name);
								var snp = why.tabSnpKonva[parseInt(event.target.name())];
								

								let subseq =why.manager._fastaLoader.getSeqAround(parseInt(snp.fromAbs),snp.alternate);
								let server = "https://covid19seqsearch.pasteur.cloud/?seq=";
								subseq = subseq.toUpperCase();
								console.log("open "+server+subseq+"#seqresults");   // #seqresults anchor to res
								why.copyStringToClipboard(name);
								window.open(server+subseq, '_blank');
							} );

						}     
					}



	        		//context.fillRect(x,0,1,10);
	        	}
	    	}

	        /*//colorSnp ="#ff69b4";
	        var xmlns = "http://www.w3.org/2000/svg";
			var rect = document.createElementNS(xmlns, "rect");
	        //var rect = document.createElementNS(svgNS,'rect');
	        rect.setAttribute("fill",colorSnp);
	        
	        //console.log(this.convert(snpi.fromAbs));
	        rect.setAttribute('x',this.convert(snpi.fromAbs));
	        rect.setAttribute('y',0);
			rect.setAttribute('width',1);
			rect.setAttribute('height',10);
			

			//rect.setAttribute('class',"ui-selectee");
			///rect.setAttribute('fill',colorSnp);
			//console.log("appendRect "+rect);
			if ((snpi.freq > this.freqMin)&&(snpi.freq < this.freqMax)){
	        	this.parentSVG.appendChild(rect);
	    	}*/
        }
        //context.stroke();
        //this.parentCanvas.stroke();
	    
		// draw Fasta Sequence
		if(this.fin-this.debut<=30){
			//console.log("draw Fasta Sequence !!!");
			//console.log(this._fasta.getSubSeq(this.debut,this.fin));
			let seq = this.manager._fastaLoader.getSubSeq(this.debut,this.fin);
			//console.log(this.debut+" "+this.fin+" "+seq);
			//console.log("seq for the Ref");
			
			for (var j=0;j<snps.length;j++){
					
					console.log(snps[j].fromAbs-Math.ceil(this.debut));
				
			}

			for(var i=0;i<seq.length;i++){
				let isSNPatthisPos =false;

				for (var j=0;j<snps.length;j++){
					if(snps[j].fromAbs-Math.ceil(this.debut) == i){
						isSNPatthisPos = true;
						console.log(i+" "+snps[j]);
						break;
					}
				}
				if(!isSNPatthisPos){
					let letter = seq.substring(i, i+1);
					// Math.ceil plus petit entier super ou egal au nombre donné
					var x= this.convert(Math.ceil(this.debut)+i)-this.convert(this.debut);

					//console.log(Math.ceil(this.debut)+ " x "+x+" "+letter);
					
					var simpleText = new Konva.Text({
								x: x,
								y: y,
								text: letter,
								fontSize: 10,
								fill: 'black',
								alpha:0.5
								});
					this.manager.parent._layerSnp.add(simpleText);
				}
			}
		}

	}
	copyStringToClipboard (string) {

        function handler (event){
        	event.clipboardData.setData('text/plain', string);
            event.preventDefault();
            document.removeEventListener('copy', handler, true);
        }

        //console.log("copyStringToClipboard");
        document.addEventListener('copy', handler, true);
        document.execCommand('copy');
    }



	filterSnpstoExport(isFreq,isHeatmap){

		//console.log("computeMapCanvas");
		//this.createScale(1);
		//var isExported =false;
		var snpsToExport =[];
		var snps = this.regionSnps(this.debut,this.fin);
		var dictSnpHeatmap = this.manager.heatmapSnpDict;
		for (var i=0;i<snps.length;i++){
			var snpi = snps[i];

			var heatKey = snpi.fromAbs+snpi.alternate;
	        var heatValue = dictSnpHeatmap[heatKey];

			if ((snpi.freq >= this.freqMin)&&(snpi.freq <= this.freqMax)){
	        	//context.rect(x,0,1,10);
	        	//context.fill();
	        	if ((heatValue > this.heatMapMin)&&(heatValue < this.heatMapMax)){
					if (snpi.coverage >= this.coverMin){
	        			snpsToExport.push(snpi);
					}
	        	}
	    	}
		}
		return snpsToExport;
		
	}
	computeMap(){

		//console.log("computeMap");

		snps = this.regionSnps(this.debut,this.fin);


		for (var i=0;i<snps.length;i++){
			var snpi = snps[i];
			if (snpi.gene != undefined ){
		                
	            if (snpi.codW == snpi.codM){
	                colorSnp = "#ff69b4"; 
	            }
	            else{
	                colorSnp = "#0000ff";   
	            }
	        }
	        else{
	           // console.log(d.from+" "+d.nucW+" "+d.gene);
	            colorSnp ="#000000";
	            
	        }
	        
	        var xmlns = "http://www.w3.org/2000/svg";

	        var c=this.parentSVG;
			var ctx=c.getContext("2d");


			ctx.beginPath();
			ctx.lineWidth="1";
			ctx.strokeStyle=colorSnp;
			ctx.strokeStyle="#00FF00";

			ctx.rect(snpsmap.convert(snpi.from),0,1,10); 
			ctx.stroke();
			
	
        }

	}

	// create Tipped tooltip
	tip(type,title){
		console.log(Tipped);
		console.log("tip");
		Tipped.init();
		
		
		Tipped.create('#subMap', {
		  inline: 'snpTip',
		  skin: 'light',
		  radius: false,
		  padding: false,
		  title: title,
		  size: 'large',
		  behavior:type
		});
	  
	  
	}
}

export default SnpMap;