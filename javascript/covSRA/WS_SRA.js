class WS_SRA {
		
    //this.genomesData="";

    /*constructor(val){
		this.genomesData="";
	}*/

    get newClientPromise() {
      return new Promise((resolve, reject) => {
        let wsClient = new WebSocket("wss://transipedia-ws-genomes.pasteur.cloud/");
        console.log(wsClient)
        wsClient.onopen = () => {
          console.log("connected");
          resolve(wsClient);
        };
        var genomesData = "";

        wsClient.onmessage = function(event) {
            console.log("got genomes result: " + event.data);
            genomesData += event.data + "<br>";
            if (genomesData.includes("end of"))
            {
                //this.getGenomesInfo(genomesData);
                // trigger the normal websocket query
                //ws.send(document.getElementById('outMsg').value);
            }
        }

        wsClient.onerror = error => reject(error);
      })
    }
    get clientPromise() {
      if (!this.promise) {
        this.promise = this.newClientPromise
      }
      return this.promise;
    }
    getGenomesInfo(genomesData)
	{
			var client2 = new XMLHttpRequest();
			client2.open('GET', '/genomes/list_unitigs_bigd.v1');
			client2.onreadystatechange = function() {
				if (client2.readyState === 4) // request is done
				{
					displayGenomes(genomesData, client2.responseText);
				}
			}
			client2.send();
	}
}

export default WS_SRA;