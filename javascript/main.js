	//import $ from 'jquery';
	//import "jquery-ui-dist";

	// https://stackoverflow.com/questions/47968529/how-do-i-use-jquery-and-jquery-ui-with-parcel-bundler
	var jquery = require("jquery");
	window.$ = window.jQuery = jquery; // notice the definition of global variables here

	
	require("jquery-ui-dist/jquery-ui.js");
	import 'jquery-ui-bundle/jquery-ui.css';

	require('jquery-colorbox');
	
	import Phylocanvas from 'phylocanvas';
	import metadataPlugin from 'phylocanvas-plugin-metadata';
	Phylocanvas.plugin(metadataPlugin);

	//require( 'datatables.net-dt' )();
	//require('datatables.net-dt/css/jquery.dataTables.min.css');

	import DataTable from 'datatables.net-dt';
	import 'datatables.net-responsive-dt';
	import 'datatables.net-buttons-dt/css/buttons.dataTables.min.css';
	import 'datatables.net-buttons/js/buttons.html5.js';
	import 'datatables.net-buttons/js/buttons.print.js'; 
	import 'datatables.net-responsive-dt';
	import 'datatables.net-select-dt'; 





	// Import Datatable ---> ???
	//var dt = require( 'datatables.net' )( window, $ );
	//require( 'jszip' );
	//require( 'pdfmake' );

	/*require('pdfmake/build/pdfmake.js');
	require('pdfmake/build/vfs_fonts.js');
	

	require( 'datatables.net-dt' )();
	require( 'datatables.net-buttons-dt' )();
	require( 'datatables.net-buttons/js/buttons.html5.js' )();
	require( 'datatables.net-buttons/js/buttons.print.js' )();
	require( 'datatables.net-responsive-dt' )();
	require( 'datatables.net-select-dt' )();

	
	import 'datatables.net-dt/css/jquery.datatables.css';
	import 'datatables.net-responsive-dt/css/responsive.dataTables.css';
	import 'datatables.net-select-dt/css/select.dataTables.css';
	import 'datatables.net-buttons-dt/css/buttons.dataTables.css';*/




    import Konva from 'konva'

    import MapManager from "./MapManager";
	import DragDrop from "./DragDrop";
	import InterfaceElement from "./InterfaceElement";

	import WS_SRA from "./covSRA/WS_SRA";

	const path = require('path');

    //var mapLength = 30000;  // Must be dynamic !!!
	//var mapLength = 11500;  
	//var mapLength = 12100; 
	var mapLength = 1000; 

	
    
    //var mapWidth = 60*($.document.width()-100)/100;
    //var mapWidth = 1000; 
	var mapWidth = document.body.clientWidth-500;   

	let dropAreadiv;


	var canvas;
	var canvasSnp;
	var stageSnpMap;
    var stageRefMap;

    var layerSnp;
    var layerRef;
    var layerNames;

    var tooltipLayer;

	var manager;
	
	var dictSnpToExport = {};

	var table; // dataTable;
    

	//console.log($("drop-area"));	
	//console.log(window);
	//window.removeDropArea();

	//var refMap2 =$("#refMap");

	//console.log($(refMap).width());
	//console.log((refMap2[0]).width());

	//console.log(refMap2);
	//$(refMap).width
	var stageRefMap = new Konva.Stage({
		container: 'refMap',
		//width: $(refMap).width(),
		width:mapWidth,
		height: 50
				
	});

	var stageSnpMap = new Konva.Stage({
		container: 'subMap',
		//width: $(refMap).width(),
		width:mapWidth,
		height: 3000,
		
	});
	/*var layer = new Konva.Layer();

	var circle = new Konva.Circle({
	x: 50,
	y: 50,
	radius: 70,
	fill: 'red',
	stroke: 'black',
	strokeWidth: 4
	});

	// add the shape to the layer
	layer.add(circle);

	// add the layer to the stage
	stageSnpMap.add(layer)*/



    var stageNames;
	var stageNames = new Konva.Stage({
		container: 'namesC',
		width: $(namesC).width(),
		height:$(namesC).height(),
			
	});
    console.log("!!!!! refmap "+stageRefMap);

	var interfaceEl = new InterfaceElement(stageRefMap,stageSnpMap,stageNames);
	interfaceEl.createCanvas();
	//interfaceEl.initElements();	
	
	
	
	

	//manager = new MapManager(window);

	//console.log(this);
	let page = this;
	//console.log(manager.canvas);
	console.log(interfaceEl.canvasSnp);
	//console.log(interfaceEl._canvasSnp);
	manager = new MapManager(interfaceEl,mapLength,10,mapWidth,"server","reference","snpsdir",canvas,canvasSnp,layerSnp,tooltipLayer,layerNames,layerRef,page);
	interfaceEl.manager = manager;

	interfaceEl.initElements();	
	//manager.init();
	dropAreadiv = document.getElementById("drop-area");
	
	var dropArea= new DragDrop(dropAreadiv,manager);
	dropArea.init();

	//console.log($("#slider-range")[0].slider);

	console.log($( "#slider-range" ));

	$(document).ready(function() {
		console.log("ICI");

	} );
	
	// Slider Frequency
    /*$( function() {
		$( "#slider-range" ).slider({
		  width:300,
		  height:50,
		  range: true,
		  min: 0,
		  max: 100,
		  values: [ 0, 100 ],
		  slide: function( event, ui ) {
			$( "#amount" ).val( "%" + ui.values[ 0 ]/100 + " - %" + ui.values[ 1 ]/100 );
			manager.setMapsFreq(ui.values[ 0 ] /100,ui.values[ 1 ] /100);
			doRedraw();
		  }
		});
		$( "#amount" ).val( "%" + $( "#slider-range" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	} );*/

	// Slider coverage
	/*$( function() {
		$( "#slider-coverage" ).slider({
		  width:300,
		  height:50,
		  min: 0,
		  max: 3000,
		  
		
		  slide: function( event, ui ) {
			$( "#coverageRange" ).val(  $( "#slider-coverage" ).slider( "value" ));
			manager.setMapsCoverage(ui.value);
			doRedraw();
		  }
		});
		$( "#coverageRange" ).val(  $( "#slider-coverage" ).slider( "value" ));
		  
	} );*/
  
	// Slider Heatmap 
	/*$( function() {
		$( "#slider-heatmap" ).slider({
		  width:300,
		  height:50,
		  range: true,
		  min: 0,
		  max: 100,
		  values: [ 0, 1000 ],	  
		  slide: function( event, ui ) {
			$( "#heatmapRange" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ]);
			manager.setMapsHeatMapRange(ui.values[ 0 ] ,ui.values[ 1 ] );
  
			doRedraw();
			}
		});
		$( "#heatmapRange" ).val( "%" + $( "#slider-heatmap" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-heatmap" ).slider( "values", 1 ) );
	} );*/
	//console.log($( "#slider-heatmap" ).slider);
	// min: 0,
	// max: 76,
	
	
	//$('#slider-heatmap').slider("option", "max", 260);
	//$( "#heatmapRange" ).val( "%%%%" + $( "#slider-heatmap" ).slider( "values", 0 ) +
	//	  " - $" + $( "#slider-heatmap" ).slider( "values", 1 ) );
	// Slider Range 
	/*$( function() {
		$( "#slider-position" ).slider({
		  width:300,
		  height:50,
		  range: true,
		  min: 0,
		  max: mapLength,
		  values: [ 0, mapLength ],
		  slide: function( event, ui ) {
			$( "#positionSlider" ).val( "bp" + ui.values[ 0 ] + " - bp" + ui.values[ 1 ]);
			manager.setMapsPosition(ui.values[ 0 ] ,ui.values[ 1 ] );
  
			doRedraw();
		  }
		});
		$( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );
	} );

	*/
	
	// color Option
    /*$( "#colorSnpFrequence" ).click(function() {
        doDraw();
    });
    $( "#colorSnpHeatmap" ).click(function() {
        doDraw();
    });
    $( "#colorSnpBasecolor" ).click(function() {
        doDraw();
    });
    $( "#colorSnpSyn" ).click(function() {
        doDraw();
    });*/

	//$(widget).add(drawbutton);

	//$( "#widget" ).dblclick(dblclickhandler);

	var wheeling;
	//$('#widget').on('mousewheel', function (e) {

	/*$('#refMap').on('mousewheel', function (e) {
		mouseWeelHandler(e);
		//prevent page fom scrolling
		return false;
	});
	// event on submap and refmap
	
	$('#subMap').on('mousewheel', function (e) {
		mouseWeelHandler(e);
		
	});*/

	$( "#save" ).click(saveImage);
	$( "#exportFasta" ).click(doExportFasta);
	$( "#test" ).click(test);
	$( "#search" ).click(search);

	/*document.getElementById("trigger").addEventListener("click", function(){ 
		doRedraw();
	});*/
	
	$( "#trigger" ).click(doExport);
	$( "#csv" ).click(exportDataCSV);
	
	/*function mouseWeelHandler(e){
		e.preventDefault();
		//console.log("! "+manager.mapRef);
		//console.log("!! "+manager.mapRef.fin);

		if(e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) { //alternative options 
			var x = e.pageX - $('#refMap').offset().left;
			var fintmp = manager.mapRef.fin - manager.mapRef.debut+1;
			interfaceEl.xGenomic = (x * fintmp) /mapWidth+manager.mapRef.debut;
			zout(e);
		} else {

			
			var x = e.pageX - $('#refMap').offset().left;
			var fintmp = manager.mapRef.fin - manager.mapRef.debut+1;
			interfaceEl.xGenomic = (x * fintmp) /mapWidth+manager.mapRef.debut;
		
			zin(e);
		}
		//prevent page fom scrolling
		return false;
	}*/


	function getIsFreq(){
		var isFreq =false;
		if($( "#frequence:checked" ).val() =='frequence'){
				isFreq =true;
			  }
		else{
		  isFreq= false;
  
		}
		return isFreq;
	  }
	function getIsHeatMap(){
		var isHeatmap =false;
		if($( "#heatmap:checked" ).val() =='heatmap'){
				isHeatmap =true;
		}
		else{
		  isHeatmap= false;
  
		}
		return isHeatmap;
	}

	function doExport(){
		
		collapse();//

		try {
			for (let map in manager.mapsSnp) {
	
					//dictSnpToExport[variable] = manager.mapsSnp[variable].filterSnpstoExport(this.getIsFreq(),this.getIsHeatMap);
					var tab =manager.mapsSnp[map].filterSnpstoExport(getIsFreq(),getIsHeatMap());   
					//var val = JSON.stringify(tab);   
					//sessionStorage.setItem(variable,val);	
					//dictSnpToExport[variable]='';
					dictSnpToExport[map]=tab;
	
			}
			//var val = JSON.stringify(dictSnpToExport);
			//sessionStorage.setItem("dictSnpToExport",val);
			//window.open("export.html"); return false;
			processExport(dictSnpToExport);
		}
		catch (e) {
			console.log(e);
			console.log("Local Storage is full, Please empty data");
			 // fires When localstorage gets full
			 // you can handle error here or empty the local storage
		}

	}

	function processExport(rows) {
        console.log("processExport rows.length "+rows);
        //console.log(rows[0]);
        //var columns =["CHR","JOSTmin","MiddlePosition","PVALmin","Region"];
        var columns =["_alternate","_coverage","_freq","_fromAbs","_reference","gene","aa","aamut"];
        tabul(rows,columns);

	}

	function tabul(rows,columns){
		if (  $.fn.DataTable.isDataTable( '#pheTable' ))
		{
			
			let table = $('#pheTable').DataTable(); 
			table.destroy();
		}
			
		let tab =new Array();
		for (let snpstrain in rows) {
			var tabSnp = rows[snpstrain];
			//console.log(tabSnp.length);
			if(tabSnp.length != 0){
				for (var i=0; i<tabSnp.length; i++) {
					var snp = tabSnp[i]; 
					tab.push(snp);
			
				}
				
			}
		}
				
			
			
		

			
		let data =  Object.keys(tab).map((key) => [tab[key]._alternate, tab[key]._coverage,tab[key]._freq,tab[key].fromAbs,tab[key]._reference,tab[key].gene,tab[key]._aa,tab[key]._aamut]);
		//let data =  Object.keys(tab).map((key) => [tab[key]._alternate, tab[key]._coverage,tab[key]._freq,tab[key].fromAbs,tab[key]._reference,'-','-','-']);
		
		console.log(data[0]);
		table = $('#pheTable').DataTable({

			data: data,
			columns: [{title: "_alternate"},{title: "_coverage"},{title: "_freq"},{title: "_fromAbs"},{title: "_reference"},{title: "gene"},{title: "aa"},{title: "aamut"}],
			order : [[ 5, "asc" ]],
			buttons: [
				{
					extend: 'csv',
					text: 'Copy all data',
					exportOptions: {
						modifier: {
							search: 'none'
						}
					}
				}
			]
		});
		

		
	}

	function tabulate(data, columns) {

		console.log(data);
		console.log("test");
		console.log(rows);
		//var table = document.createElement("table");
		table = $('#pheTable')[0];

		console.log(table);
		

		console.log(table);
		if (($.fn.DataTable !== undefined)&&(  $.fn.DataTable.isDataTable( '#pheTable' ) )) {
			  //console.log("EMPTYYYYYYYYYY");
			  table[0].DataTable().clear().draw();
			  table[0].DataTable().destroy();
			  $('#pheTable').empty();

		}
		

		   //table.id="pheTable";  
		console.log(table);
		var thead = document.createElement("thead");
		var tr = document.createElement("tr");
		
		var th = document.createElement("th");
		th.style = "font-size:10px";

		th.innerHTML ="snpStrain";
		tr.appendChild(th);
		
		for (var i=0; i<columns.length; i++) {
			console.log(columns[i]);
			  //if ((columns[i] != "ID")&&(columns[i] != "linkRef")){
			var th = document.createElement("th");
			th.style = "font-size:10px";
			th.innerHTML =columns[i];
			tr.appendChild(th);
				//}
		}
		console.log(tr);
		thead.appendChild(tr);
		table.append(thead);

		   

			var tbody = document.createElement("tbody");
			for (let snpstrain in data) {
				var tabSnp = data[snpstrain];
				console.log(tabSnp.length);
				if(tabSnp.length != 0){
					for (var i=0; i<tabSnp.length; i++) {
						var snp = tabSnp[i]; // pas l'objet mais un dictionnaire !
						
						var tr = document.createElement("tr");
						var td  = document.createElement("td");
						td.innerHTML=snpstrain+"";
						td.style ="font-size:8px";
						tr.append(td);
						
						for (var j=0; j<columns.length; j++) {
							var td  = document.createElement("td");
							td.style ="font-size:8px";
							td.innerHTML=snp[columns[j]];
							tr.append(td);
						}
						tbody.appendChild(tr);

					}
					
				}
			}
			table.append(tbody);

		console.log("creation de la table d'export :");

		table = $('#pheTable').dataTable( {
	
		  aoColumns: [ { },{ },{ },{ },{ },{ },{ },{ },{ }],
		  dom: 'Bfrtip',
		  pageLength : 20,
		  buttons: [
		  'copy', 'csv', 'pdf'
		  ],
		  "scrollY":        "200px",
		  "scrollCollapse": true,
		  "paging":         false

	
		} );

		$('#pheTable').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
			}
			else {
				table.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
			}
		} );

		 //var table = $('#pheTable').dataTable();

	  
	  
	} 	
	function exportDataCSV(){

		let data = table.data().toArray();
		let textfile="";
 
		data.forEach(function(row, i) {
  			row.forEach(function(column, j) {
   				
				//textfile+='row ' + i + ' column ' + j + ' value ' + column+'\n';
				textfile+=column+'\t';
  			});
			textfile+='\n';
		});
		const start = Date.now();
		var filename ="export"+start+".csv";

  
  
		var blob = new Blob([textfile], { type: 'text/csv;charset=utf-8;' });
		if (navigator.msSaveBlob) { // IE 10+
			  navigator.msSaveBlob(blob, filename);
		} else {
			  var link = document.createElement("a");
			  if (link.download !== undefined) { // feature detection
				  // Browsers that support HTML5 download attribute
				  var url = URL.createObjectURL(blob);
				  link.setAttribute("href", url);
				  link.setAttribute("download", filename);
				  link.style.visibility = 'hidden';
				  document.body.appendChild(link);
				  link.click();
				  document.body.removeChild(link);
			  }
		}
  
	  

	}

	// (de)collapse the export div with flex/flexbox mechanism
	function collapse(){
		console.log("export");
		console.log(document.querySelector('.flex-container-row.collapsible'));
		document.querySelector('.flex-container-row.collapsible').classList.toggle('collapsed');
	}

	/*
	function zout(e){
		
		var newX;
		var newY;
		  newX = interfaceEl.xGenomic -(interfaceEl.xGenomic-manager.mapRef.debut)*(1.2);       
		  newY = (manager.mapRef.fin-interfaceEl.xGenomic)*(1.2) + interfaceEl.xGenomic;
		  
		  $("#slider-position").slider("values" , [newX,newY]);
		  $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );
		  if((newY-newX)< mapLength){
			manager.setMapsPosition(newX,newY );
			doRedraw();
		  }
  
	}
	function zin(e){
		
		var newX;
		var newY;
	
		  
		newX = interfaceEl.xGenomic -(interfaceEl.xGenomic - manager.mapRef.debut)*0.8;
		newY = interfaceEl.xGenomic +(manager.mapRef.fin-interfaceEl.xGenomic)*0.8;  
		
		$("#slider-position").slider("values" , [newX,newY]);
		  $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );
		if(newY-newX>25){
		  manager.setMapsPosition(newX,newY);
		  doRedraw();
		}
		
	}*/

	


	/*function dblclickhandler(){
		console.log("doubleclick");
		interfaceEl.xGenomic = 0;
		//nZoom = 1;
		$("#slider-position").slider("values" , [0,mapLength]);
		  $( "#positionSlider" ).val( "bp" + $( "#slider-position" ).slider( "values", 0 ) +
		  " - bp " + $( "#slider-position" ).slider( "values", 1 ) );

		interfaceEl.goTOGenomicPosition(1,mapLength );
		
	}*/

	/*function doRedraw(){
		
		interfaceEl.doRedraw();
	}

	function doDraw(){


		console.log();
		interfaceEl.doDraw();

		console.log(manager);
		console.log(manager.strainManager);

	}
	

	function doErase(){
            
		//manager.setMapsRange(5000,12000);
		manager.mapRef.erase();

		for (var variable in manager.mapsSnp) {
		  manager.mapsSnp[variable].erase();
		}

		var rects = interfaceEl.layerSnp.getChildren(function(node){
		  return node.getClassName() === 'Text';

		});
		for (var i=0; i<rects.length; i++) {
		  //console.log(rects[i]);
		  rects[i].destroy();
		}
		var textsRef = interfaceEl.layerRef.getChildren(function(node){
		  return node.getClassName() === 'Text';

		});
		for (var i=0; i<textsRef.length; i++) {
		  //console.log(rects[i]);
		  textsRef[i].destroy();
		}
		var rectsRef = interfaceEl.layerRef.getChildren(function(node){
		  return node.getClassName() === 'Rect';

		});
		for (var i=0; i<rectsRef.length; i++) {
		  //console.log(rects[i]);
		  rectsRef[i].destroy();
		}

		interfaceEl.layerRef.draw();
		interfaceEl.layerSnp.draw();
		

	}*/


	function saveImage(){
      
		var dataURL = stageSnpMap.toDataURL({ pixelRatio: 3 });
		var dataURLRef = stageRefMap.toDataURL({ pixelRatio: 3 });
  
		//console.log(dataURL);
		//console.log(dataURLRef);
		//goTOGenomicPosition(9982,11301);
		downloadURI(dataURL, 'stage.png');
	}

	function downloadURI(uri, name) {
        var link = document.createElement('a');
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        //document.body.removeChild(link);
        //delete link;
    }
	

	function doExportFasta(){
	  
		console.log("doExportFasta");


		var isFiltering =true;
  
  
  
		if(!isFiltering){
		  var textfile =">ref\n";
  
		  for (position in manager.snpArtificialSeq){
  
			  var nuc = manager.snpArtificialSeq[position]["ref"];
			  //console.log(position+" "+nuc);
			  textfile +=nuc;
		  }
		  //textfile+="\n";
  
		  //var myKeysPos = manager.heatmapSnpDict.keys(); // ensemble positions snp
		  for (variable in manager.mapsSnp) {
  
			textfile +="\n>"+variable+"\n";
			for (position in manager.snpArtificialSeq){
			  var nuc;
			  if(variable in manager.snpArtificialSeq[position]){
				var snp = manager.snpArtificialSeq[position][variable];              
				nuc = snp.alternate;
			  }
			  else{
				nuc = manager.snpArtificialSeq[position]["ref"];
			  }
			  //console.log(position+" "+nuc);
			  textfile +=nuc;
			}
		  
			//heatmapSnpDict
			
		  }
		  textfile+="\n";
		}
		// construct the alignment with filtering options
		else{
		  var tabPosWithfilteredSnp ={}; //positions with at least one filtered Snp
		  
		  console.log("!!!!!!**** exportFasta "+manager.mapRef.debut+" "+manager.mapRef.fin);
		  console.log(manager._heatMapMin+" "+manager._heatMapMax);

		  for (let mapSnp in manager.mapsSnp) {
			for (let position in manager.snpArtificialSeq){
  
			  if((position >=manager.mapRef.debut)&&(position <= manager.mapRef.fin)){
				//console.log("range ok"+position);
  
				if(mapSnp in manager.snpArtificialSeq[position]){
				  var snp = manager.snpArtificialSeq[position][mapSnp];
				  // if pos
  
				  var heatKey = snp.fromAbs+snp.alternate;
				  var heatValue = manager.heatmapSnpDict[heatKey];
				  console.log(snp.fromAbs+" "+snp.alternate+" "+snp.freq+" "+manager._freqMin+" "+manager._freqMax+" "+heatValue);
				  if ((snp.freq >= manager._freqMin)&&(snp.freq <= manager._freqMax)) {
				  
					if ((heatValue > manager._heatMapMin)&&(heatValue < manager._heatMapMax)) {
					  tabPosWithfilteredSnp[position]="";     
					  console.log("add pos "+position);
					}
				  }
				}
			  }
			}
		  }
  
		  var textfile =">ref\n";
  
		  for (let position in tabPosWithfilteredSnp){
			  console.log("pos "+position);
			  var nuc = manager.snpArtificialSeq[position]["ref"];
			  //console.log(position+" "+nuc);
			  textfile +=nuc;
		  }
  
		  for (let variable in manager.mapsSnp) {
  
  
			textfile +="\n>"+variable+"\n";
			for (let position in tabPosWithfilteredSnp){
  
			  var nuc;
			  if(variable in manager.snpArtificialSeq[position]){
				var snp = manager.snpArtificialSeq[position][variable];
				//if ((snp.freq >= manager.freqMin)&&(snp.freq <= manager.freqMax)){
				
				  //if ((heatValue > manager.heatMapMin)&&(heatValue < manager.heatMapMax)){
					 nuc = snp.alternate;
				  
				  //}
				//}
			  }
			  else{
				nuc = manager.snpArtificialSeq[position]["ref"];
			  }
			  //console.log(position+" "+nuc);
			  textfile +=nuc;
			}
		  
			//heatmapSnpDict
			
		  }
		  textfile+="\n";
  
  
		}
  
		// create the fasta file
  
		var filename ="export.fasta";
  
  
  
		var blob = new Blob([textfile], { type: 'text/csv;charset=utf-8;' });
		  if (navigator.msSaveBlob) { // IE 10+
			  navigator.msSaveBlob(blob, filename);
		  } else {
			  var link = document.createElement("a");
			  if (link.download !== undefined) { // feature detection
				  // Browsers that support HTML5 download attribute
				  var url = URL.createObjectURL(blob);
				  link.setAttribute("href", url);
				  link.setAttribute("download", filename);
				  link.style.visibility = 'hidden';
				  document.body.appendChild(link);
				  link.click();
				  document.body.removeChild(link);
			  }
		  }
  
	  }

	  function updateMapLength(mapL){
		mapLength = mapL;
		
	  }

	  

	  function test(){

		console.log("test");

		for (let map in manager.mapsSnp) {
	

			console.log(map);
			console.log(manager.mapsSnp[map]);
			//dictSnpToExport[variable] = manager.mapsSnp[variable].filterSnpstoExport(this.getIsFreq(),this.getIsHeatMap);
			//var tab =manager.mapsSnp[map].filterSnpstoExport(getIsFreq(),getIsHeatMap());   


			//var val = JSON.stringify(tab);   
			//sessionStorage.setItem(variable,val);	
			//dictSnpToExport[variable]='';
			//dictSnpToExport[map]=tab;

		}

	  }
	// to move in a class

	function search(){

		//$(".inline").colorbox();
		$.colorbox({href:"#inline_content",inline:true, width:"80%"});
		/*$.colorbox({onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
	  });*/
		var msg="TCAAATTGGATGACAAAGATCCAAATTTCAA";

		/*var wsGenomes = new WebSocket('wss://transipedia-ws-genomes.pasteur.cloud/');
		console.log(wsGenomes);
		var genomesData = "";
		//wsGenomes.onmessage = function(e){ console.log(e.data); };
		wsGenomes.onmessage = function(event) {
			console.log("got genomes result: " + event.data);
			genomesData += event.data + "<br>";
			if (genomesData.includes("end of"))
			{
				getGenomesInfo(genomesData);
				// trigger the normal websocket query
				ws.send(document.getElementById('outMsg').value);
			}
		}
		wsGenomes.send(msg);*/

		let wsSingleton = new WS_SRA();
		var genomesData = "";
		

	    wsSingleton.clientPromise
        .then( wsClient =>{wsClient.send(msg); console.log(wsClient);console.log('sended')})
        .catch( error => alert(error) )
		
	}

	handleFileList();

	// take arg from the url http://server/?dataDir="Data"
	function searchToObject(search) {
        return search.substring(1).split("&").reduce(function(result, value) {
          var parts = value.split('=');
          if (parts[0]) result[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
          return result;
        }, {})

    }

	function getServer(){
		var parameters = location.search;
		let param = searchToObject(parameters);
		let dataDir = "Data";
		console.log(param.dataDir);
		if ((param.dataDir != "")&&(param.dataDir !== undefined )){
			dataDir = param.dataDir.replace(/^"|"$/g, '');
		}
		console.log(param.dataDir);
		let server = window.location.origin+window.location.pathname+"/"+dataDir+"/";	
		return server;
	}

	function sleep(miliseconds) {
		var currentTime = new Date().getTime();
		while (currentTime + miliseconds >= new Date().getTime()) {
		}
	}
	async function handleFileList(){

		//let server = "http://hub18.hosting.pasteur.fr/SynTView/Data/Yersinia/yersinomics/pestis/";
		//server = "http://localhost/devPoo/SynTView/Data/";
		
		//let server = window.location.href+"Data/";
		//let server = window.location.origin+window.location.pathname+"/"+dataDir+"/";
		let server = getServer();
		
		console.log(server);
		let file = "liste";

		let url = server+file;
		
		console.log(url);


		//var file_list = [...files];
		let promises = [];

		
		
		//for (let file of file_list) {
		let filePromise = new Promise(resolve => {
			let request = new XMLHttpRequest();
			request.open('GET', url, true);
			//request.send(null);
			//console.log(request.responseText);

			

			//request.onreadystatechange = () => resolve(new Array("liste",request.responseText));
			request.onload = () => resolve(request.responseText);
			request.send(null);
		});
			

			//promises.push(filePromise);


		//}
		let textIn;
		//Promise.allSettled(promises).then(textIn => {
	
	
		filePromise.then((txt) => {
			//console.log(txt);

			// !!!! Liste file begin with ptt file 
			if((txt.split("\n")[0]).split(".")[1]== "ptt"){
				manager.init();
				standbyAnimation(txt);
			}
			//sleep(2000);
			

			//handleFiles(txt);
		});
	}




	async function standbyAnimation(txt){	
		var htmlCanvas = document.getElementById('canvas');
		console.log("canvas");

		var htmlCanvasjq = $('canvas');
		console.log(htmlCanvasjq);
		htmlCanvas.width = 1500;
		htmlCanvas.height = 800;


		let w;
		let offscreen;
		var hasOffscreenSupport = !!htmlCanvas.transferControlToOffscreen;
		if (hasOffscreenSupport) {
			offscreen = htmlCanvas.transferControlToOffscreen();
			manager._worker = new Worker(path.dirname()+'/workerStandBy.js');
			await manager._worker.postMessage({ canvas: offscreen }, [offscreen]);
			// wait for animation
			//load all the files
			handleFiles(txt);


		}
		else {
			/*htmlCanvas
			.getContext('2d')
			.fillText(
				'🛑 Sorry, your browser does not support Offscreen rendering...',
				20,
				20
			);*/
			handleFiles(txt);
		}
	}

	async function handleFiles(textIn){

		console.log(window.location.href);
		await manager.resolveAfter1Second();
		//let server  = "http://localhost/devPoo/SynTView/Data/";
		//let server = window.location.href+"Data/"
		let server = getServer();
		let promises= [];
		
		console.log(textIn);		
		var lines = textIn.split("\n");

		var numLines = lines.length-1;
			/* */ 
		console.log(numLines);
		for (var i=0; i< numLines; i++) {  //>
			console.log(lines[i]);

			let fileP = new Promise(resolve => {
					let request = new XMLHttpRequest();
					let filename = lines[i];
					let url = server + filename;
					
					//url  ="http://localhost/devPoo/SynTView/Data/GCF000006645.ptt"
		
					console.log(url);
					request.open('GET', url, true);
					request.send(null);
					//request.onreadystatechange = () => resolve(request.responseText);
					request.onload = () => resolve(new Array(filename,request.responseText));
					//request.onload = () => resolve(request.responseText);
					
					
			});
			promises.push(fileP);
		}

				/*let fileP = new Promise(resolve => {
					const xhr = new XMLHttpRequest();
					url = server + lines[i];
					xhr.onload = () => {
					  //if (xhr.status === 200) {
						resolve(xhr.responseText); // (A)
					  //} 
					}
					//xhr.onerror = () => {
					//  reject(new Error('Network error')); // (C)
					//};
					xhr.open('GET', url);
					xhr.send();
				});
				
			};
		
		});*/

		//console.log(promises);
		//Promise.allSettled(promises).then((values) => {
		//	console.log(values);
		//});
		//Promise.allSettled(promises).
        //    then((results) => results.forEach((result) => console.log(result.status)));

		
		Promise.all(promises).then(fileContents => {


			console.log("fini ");
			
			manager.processfiles(fileContents);
			/*for (let tab of fileContents) {

				console.log(tab);
				let filename = tab[0];
				let filetxt = tab[1];

				console.log(filename);
				console.log(filetxt);
			}*/

		});


		
	}


	  



	

	
	



