import GenomicMap from "./GenomicMap";
import MapManager from "./MapManager";

var jquery = require("jquery");
window.$ = window.jQuery = jquery;
var Tipped = require("@staaky/tipped");
import '../tipped.css';
import { map } from "jquery";


class GenomeMap extends GenomicMap{

	constructor(canvas,mapL,mapH,mapW,repliconL,title,manager){
		super(canvas,mapL,mapH,mapW,repliconL,title,manager);
		this.genes=[];

		this._fasta = null;
		this.seed =1;

	}

	regionGenes(posfrom,posto){
				
		var gs =[];
				
		//console.log(posfrom+" "+posto+" "+this.genes.length);
		for (var i=0;i<this.genes.length;i++){
			var gene = this.genes[i] ;	
			//console.log(gene.fromAbs+" "+gene.toAbs);
			if ((gene.fromAbs >= posfrom) && (gene.fromAbs <=posto)){
				//trace(gene.accNum);
				//console.log(gene.accNum);
				gs.push(gene);	
			}
			else if ((gene.toAbs > posfrom) && (gene.fromAbs <=posfrom)){
				gs.push(gene);	
			}
		}
		//console.log(gs+" "+gs.length);
		return gs;
		
	}
	createScale(isgraduate,height){
		
		   	console.log(this);
			var context = this.parentCanvas.getContext('2d');
			context.lineWidth="1";
		    var inter = this.mapLength/10;
		    context.fillStyle='#eceff4';

		    //console.log("inter "+inter+" "+this.mapLength);
		    console.log( "createScale "+this.mapHeight);
		    for (var i=0;i<10;i++){
		    	if (i/2 -Math.round(i/2) ==0){
		    		//context.fillRect(this.convert(i*inter),0,this.convert(inter),this.mapHeight);
		    		context.fillRect(this.convert(i*inter),0,this.convert(inter),height);		    		
		    	}

		    }
		  	//document.body.appendChild(svg);
	}
	computeMap(){

		
		var context = this.parentCanvas.getContext('2d');


		context.lineWidth="1";
		//context.fillStyle= '#ff0000';
			
		//this.createScale(1);

		//if(this.fin-this.debut>15000){
			var gs = this.regionGenes(this.debut,this.fin);

			for (var i=0;i<gs.length;i++){
				var gene = gs[i];
				
				var xvar = this.convert(gene.fromAbs-this.debut);
				var rectLength = gene.length;
				//last gene toAbs longer than this.end
				if (gene.toAbs > this.fin){
					rectLength = this.fin-gene.fromAbs+1;
				}

				var widthvar = this.convert(rectLength);
				//if 
	    		//context.fillStyle= this.getRandomColor();
	    		if(gene.color==""){
	    			gene._color= this.getRandomColor();
	    		}
	    			
	    		context.fillStyle= gene.color;

	    		var yvar;
	    		//console.log("DIR "+gene.direction);
	    		//console.log(gene);
	    		if (gene.direction == '+'){
	    			yvar = this.rank*this.mapHeight;
	    		}
	    		else{
	    			//yvar = this.rank*this.mapHeight +this.mapHeight/2;
	    			yvar = this.rank*this.mapHeight *2;
	    		}
	    		if(this.fin-this.debut>50000){
	    			context.fillRect(xvar,yvar,widthvar,10);
	    		}
	    		// create Rect 
	    		else{


	    			/*var tooltipLayer = manager.tooltiplayer;
	    			var stageTip = tooltipLayer.getStage();
        			var tooltip = new Konva.Text({
				        text: "",
				        fontFamily: "Calibri",
				        fontSize: 12,
				        padding: 5,
				        textFill: "white",
				        fill: "black",
				        alpha: 0.75,
				        visible: false,
				        
				    });


				    tooltipLayer.add(tooltip);*/
	    			var rect = new Konva.Rect({
				      x: xvar,
				      y: yvar,
				      width: widthvar,
				      height: 10,
				      fill: gene.color,
				      stroke: 'black',
				      strokeWidth: 1,
				      opacity: 0.8,
				      name: gene.id
				    });
	    			this.manager.parent.layerRef.add(rect);


					var why = this;
	    			rect.on('mouseover', function() {
			            this.stroke('blue');
			            this.strokeWidth(3);
			            why.manager.parent.layerRef.draw();



			        });

	    			

			        
					rect.on('mousemove', function(event) {

					
						//console.log("mouse move");
						//console.log(event.target);
						//console.log(event.target.name());
	
						//var gene = why.tabSnpKonva[parseInt(event.target.name())]
	
						var gene = why.manager.hashNameGenes[event.target.name()];
						//console.log(gene);
						$(".geneTip").text(gene.id);
						$(".coordTip").text(gene.fromAbs+" "+gene.toAbs);
						
						$(".genenameTip").text(gene.name);
						$(".synonymousTip").text(gene.synonyme);
						$(".geneproductTip").text(gene.product);
						//$(".strainTip").text(gene._org._name);
						$(".strandTip").text(gene.direction);
						
						$(".ycoordinate").text(yvar);
						//console.log(gene);
						//console.log(gene.id);
						why.tip('mouse',gene.id);
						Tipped.show('#refMap');
	
					});

					rect.on('mouseout', function() {
						//console.log("mouseleave");
						this.stroke('black');
						this.strokeWidth(1);
							
						
						//Tipped.disable('#container');
						Tipped.hide('#refMap');
						Tipped.disable('#refMap');
						$(".geneTip").text("-");
						$(".coordTip").text("-");
						
						$(".genenameTip").text("-");
						$(".synonymousTip").text("-");
						
						$(".geneproductTip").text("-");
						$(".ycoordinate").text("-");
	
					
						why.manager.parent.layerRef.draw();
					
					});


			        rect.on('click', function(e) {
			            //manager.setMapsPosition(gene.fromAbs ,gene.toAbs);
			            //console.log(e.target.name());
			            var myGene = why.manager.hashNameGenes[e.target.name()];
			            console.log(myGene.id+" "+myGene.fromAbs +" "+myGene.toAbs+" iii");
			            let xG =Number.parseInt(myGene.fromAbs);
			            let yG =Number.parseInt(myGene.toAbs);
			            why.manager.parent.goTOGenomicPosition(myGene.fromAbs-1,myGene.toAbs+1);
			            //manager.page.goTOGenomicPosition(9982,11301);

			        });


			        
	    		}
		    			    	
			}
		
		/// COV
		/*var posAngl = [3267,5388,6954,11288,21765,21991,23063,23271,23604,23709,24506,24914,27972,28048,28111,28280,28977];
		for(var i=0;i<posAngl.length;i++){
			//console.log(posAngl[i]+" "+ this.debut+" "+this.fin);
			if((posAngl[i]>=this.debut)&&(posAngl[i]<=this.fin)){
				//console.log("!!! "+posAngl[i]+" "+ this.debut+" "+this.fin);
				var triangle = new Konva.RegularPolygon({
					x: this.convert(posAngl[i]-this.debut),    //-this.debut;
					y: this.mapHeight *3.5,
					//y: this.mapHeight,
					sides: 3,
					radius: 3,
					fill: '#00D2FF',
					stroke: 'black',
					strokeWidth: 0.5,
				  });
				this.manager.parent.layerRef.add(triangle);
			}
			
		}*/


		// draw Fasta Sequence
		if(this.fin-this.debut<=100){
			//console.log("draw Fasta Sequence !!!");
			//console.log(this._fasta.getSubSeq(this.debut,this.fin));
			let seq = this.manager._fastaLoader.getSubSeq(this.debut,this.fin);
			//console.log(this.debut+" "+this.fin+" "+seq);
			//console.log("seq for the Ref");
			for(var i=0;i<seq.length;i++){

				let letter = seq.substring(i, i+1);
				// Math.ceil plus petit entier super ou egal au nombre donné
				var x= this.convert(Math.ceil(this.debut)+i)-this.convert(this.debut);

				//console.log(Math.ceil(this.debut)+ " x "+x+" "+letter);
				
				var simpleText = new Konva.Text({
						      x: x,
						      y: 25,
						      text: letter,
						      fontSize: 10,
						      fill: 'black'
						    });
			  	this.manager.parent._layerRef.add(simpleText);
			}
		}

	}
	// create Tipped tooltip
	tip(type,title){
		//console.log(Tipped);
		//console.log("tip");
		Tipped.init();
		
		
		Tipped.create('#refMap', {
			inline: 'geneTip',
			skin: 'light',
			title: title,
			size: 'medium',
			position:'topleft',
			behavior:type
		});
		

		
	}
	computeMapOld(){
		var context = this.parentCanvas.getContext('2d');
		//console.log("mapLength "+this.mapLength + "debut " +this.debut +"fin " + this.fin );
		//console.log("debut "+this.debut,this.fin);
		var gs = this.regionGenes(this.debut,this.fin);
		//console.log("parentSVG "+ this.parentSVG);
		var svgNS = this.parentSVG.namespaceURI;

		for (var i=0;i<gs.length;i++){
			var gene = gs[i];
			//.log("from to "+gene.fromAbs+" "+gene.toAbs);
			var rect = document.createElementNS(svgNS,'rect');

			rect.setAttribute('fill','#ff0000');
			var xvar = this.convert(gene.fromAbs-this.debut);

			var widthvar = this.convert(gene.length);
			//console.log(xvar+" "+widthvar+" ii"+gene._direction+"ii");
    		rect.setAttribute('x',xvar);
    		var yvar;
    		//console.log(gene.direction);
    		if (gene.direction == '+'){
    			yvar = this.rank*this.mapHeight;
    		}
    		else{
    			yvar = this.rank*this.mapHeight +this.mapHeight/2;
    		}

    		rect.setAttribute('y',yvar);
    		rect.setAttribute('width',widthvar);
    		rect.setAttribute('height',10);
    		this.parentSVG.appendChild(rect);
		}

	}
	redraw(posfrom,posto){
		computeMap(posfrom,posto);
	}

	erase(){
		//console.log("erase");
		/*while (this.parentSVG.lastChild) {
    		this.parentSVG.removeChild(this.parentSVG.lastChild);
		}*/

		this.parentCanvas.getContext("2d").clearRect(0, 0, this.parentCanvas.width, this.parentCanvas.height);

	}
	random() {
		var x = Math.sin(this.seed++) * 10000;
		return x - Math.floor(x);
	}
	getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    // color += letters[Math.floor(Math.random() * 16)];
		color += letters[Math.floor(this.random() * 16)];
	  }
	  return color;
	}
}


export default GenomeMap;