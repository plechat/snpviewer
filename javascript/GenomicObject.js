class GenomicObject{
	constructor(begin,end,direction){
		this._fromAbs=begin;
		this._toAbs=end;
		this._direction= direction;
		this._length;
	}
	
	
	get fromAbs(){
		return this._fromAbs;
	}
	get toAbs(){
		return this._toAbs;
	}
	get direction(){
		return this._direction;
	}
	get length(){
		return this._length;
	}

}


export default GenomicObject;