//import Snp from "./Snp";

class DelLoader{
	
	constructor(strainName,delMap,manager){
		this._data =null;

		this._strainName = strainName;
		this._delsmap = delMap;
		this.manager =manager;

		this._tabDel =new Array(); 
		this._tabMoyDel= new Array();
	}
	

	get data(){
		return this._data;
	}
	parseSNPfile(){

		console.log("parseSNPfile");

		//console.log(this.data);
		var org = new Organism("test");


		return org;

	}

	loadDels(textIn){


		for (var i=1; i<this.manager.mapLength; i++) {
			
			this._tabDel[i]= 0;
		}

		
		var lines = textIn.split("\n");

		var numLines = lines.length-1;
		//console.log(numLines);

		var manager = this.manager;

		
		for (var i=1; i<numLines; i++) {

			//console.log(lines[0]);
			var tabs = lines[i].split("\t");

			
	        let start = tabs[0];
			let stop = tabs[1];
			let delSize = tabs[1];
			let cover = tabs[3];
			

			console.log(start+" "+ stop )

            for(var j=start; j<=stop; j++) {
				this._tabDel[j]=this._tabDel[j]+1;

			}
			
		}
		
		
		this._delsmap._tabDel = this._tabDel;

		//return new Promise(resolve => true);

	}



	async load(url) {

	  	var why =this;
	  	return new Promise(function(resolve, reject) {
		    const xhr = new XMLHttpRequest();
		    xhr.onreadystatechange = function(e) {
		      if (xhr.readyState === 4) {
		        if (xhr.status === 200) {
		          why._data = xhr.response;
		          resolve(why);

		        } else {
		          reject(xhr.status);
		        }
		      }
		    }
		    xhr.ontimeout = function () {
		      reject('timeout');
		    }
		    xhr.open('get', url, true);
		    xhr.send();

		    
		});
	}

	
}

export default DelLoader;