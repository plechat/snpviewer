import BioTools from "./BioTools";
import PttLoader from "./PttLoader";
import SnpLoader from "./SnpLoader";
import DelLoader from "./DelLoader";
import FastaLoader from "./FastaLoader";
import TabulatedGroupLoader from "./TabulatedGroupLoader";
import TreeLoader from "./TreeLoader";

import GenomeMap from "./GenomeMap";
import SnpMap from "./SnpMap";
import DelMap from "./DelMap";




class MapManager{

	constructor(parent,mapL,mapH,mapW,server,reference,snpsdir,canvas,canvasSnp,layerSnp,layerNames,page){
		
		this._files;
		this.parent = parent;   // InterfaceELement

		this.mapLength = mapL;
		this.mapHeight =mapH;   // classes de constantes
		this.mapWidth = mapW;
		
		this.mapRef =null;
		this.canvasRef=parent.canvas;
		//this.canvasSNP=canvasSnp;
		this.canvasSNP = parent.canvasSnp;

		//this.svgRef=null;
		this.mapsSnp=[];
		//this.repliconL=21000;
		this.repliconL=30000;
		this.decal=15;

		this.server=server;         // web server 
    	this.reference=reference;   // reference genome
    	this.snpsdir =snpsdir;      // snps files web server directory

    	this._strainsNumber=0; 		   // number of snps strains

    	this._heatmapSnpDict={};    // heatmap with color by snp position key -> snp+nuc mutated
    	this._posSnpDict={};        // dict with all snp pos -> tab ATGC -> [1,5,8,0]
    	this._colorHeatmap = [];    // color gradiant between 2 colors with strainsNumber steps
    	this._snpArtificialSeq ={}; //position -> dictionnary (souche ) -> snp1

    	this._dictYcoordStrain = {};// strain --> pos y : if a tree -> Phylocanvas --> y

    	this._freqMin=0;
    	this._freqMax=1;

    	this._heatMapMin=0; // -> 1 ?
    	this._heatMapMax=100;
    	// !!! REFACTOR Move To Map
    	this._layerSNP= layerSnp;
    	//this._tooltiplayer =tooltiplayer;
    	this._layerNames = layerNames;
    	//this._layerRef = layerRef;

    	this._biotool = new BioTools();
    	this._hashNameGenes = {};
    	
		this._page =page;

		this._fastaLoader;
		this._grpLoader;
		this._treeLoader;

		this._coverMin =0;

		this._strainManager;
		
		this._tree; // phylocanvas tree;


	}

	get page(){
		return this._page;
	}

	get hashNameGenes(){
		return this._hashNameGenes;
	}
	get layerNames(){
		return this._layerNames;
	}

	get layerSNP(){
		return this._layerSNP;
	}
	get colorHeatmap(){
		return this._colorHeatmap;
	}

	set colorHeatmap(cols){
		this._colorHeatmap=cols;
	}

	get snpArtificialSeq(){
		return this._snpArtificialSeq;
	}

	set snpArtificialSeq(dict){
		this._snpArtificialSeq=dict;
	}

	get dictYcoordStrain(){
		return this._dictYcoordStrain;
	}

	set dictYcoordStrain(dict){
		this._dictYcoordStrain=dict;
	}


	get heatmapSnpDict(){
		return this._heatmapSnpDict;
	}

	set heatmapSnpDict(dict){
		this._heatmapSnpDict=dict;
	}

	get posSnpDict(){
		return this._posSnpDict;
	}

	set posSnpDict(dict){
		this._posSnpDict=dict;
	}

	get strainsNumber(){
		return this._strainsNumber;
	}
	set strainsNumber(n){
		this._strainsNumber=n;
	}

		// filters
	
	get freqMin(){
		return this._freqMin;
	}
	set freqMin(f){
		this._freqMin=f;
	}

	get freqMax(){
		return this._freqMax;
	}
	set freqMax(f){
		this._freqMax=f;
	}

	get rangeMin(){
		return this._freqMin;
	}
	set rangeMin(r){
		this._rangeMin=r;	
	}

	get rangeMax(){
		return this._freqMax;
	}
	set rangeMax(r){
		this._rangeMax=r;	
	}

	get strainManager(){
		return this._strainManager;
	}
	set strainManager(sman){
		this._strainManager=sman;	
	}



	init(){
		console.log("init");
		// remove the dropArea
		console.log(this);
		console.log(this.parent);
		console.log(this.page);
		console.log(document.getElementById("widget"));

		this.parent.removeDropArea();
		//this.parent.createLayout();
		//this.parent.createCanvas();

		// Load and parse all the files

		//this.handleFiles(this._files);
		//this.handleFilesRN(this._files);

		



	}

	get files(){
		return this._files;
	}
	set files(files){
		this._files= files;
	}

	setMapsRange(debut,fin){
		this.mapRef.debut =debut;
		this.mapRef.fin =fin;

		for (var snpstrain in this.mapsSnp) {
              //console.log("tt" +variable);
              this.mapsSnp[snpstrain].debut =debut;
              this.mapsSnp[snpstrain].fin =fin;

              //manager.mapsSnp[variable].computeMapSVG();

        }

	}
	setMapsFreq(freqMin,freqMax){
		for (var snpstrain in this.mapsSnp) {
              this.mapsSnp[snpstrain].freqMin =freqMin;
              this.mapsSnp[snpstrain].freqMax =freqMax;
        }
        this._freqMin=freqMin;
    	this._freqMax=freqMax;
	}

	setMapsCoverage(coverMin){
		for (var snpstrain in this.mapsSnp) {
              this.mapsSnp[snpstrain].coverMin =coverMin;
        }
        this._coverMin=coverMin;
	}


	setMapsHeatMapRange(rangeMin,rangeMax){

		for (var snpstrain in this.mapsSnp) {
              this.mapsSnp[snpstrain].heatMapMin =rangeMin;
              this.mapsSnp[snpstrain].heatMapMax =rangeMax;
        }
        this._heatMapMin=rangeMin;
    	this._heatMapMax=rangeMax;
	}

	setMapsPosition(posMin,posMax){

		this.mapRef.debut =posMin;
		this.mapRef.fin =posMax;
		var maplength =posMax-posMin+1;

		this.mapRef.mapLength = maplength;
		var conv = this.mapRef.mapWidth/maplength;
		//console.log("CCCC "+conv);
		this.mapRef.conv=conv;
		for (var snpstrain in this.mapsSnp) {
              this.mapsSnp[snpstrain].debut =posMin;
              this.mapsSnp[snpstrain].fin =posMax;
              this.mapsSnp[snpstrain].mapLength = maplength;
              this.mapsSnp[snpstrain].conv  = conv;

        }
        
	}



	// NEW 
	async handleFilesRN(files){
		await this.resolveAfter1Second();
		var file_list = [...files];
		let promises = [];

		//let valueProgress =0;

		let why = this;
		
		for (let file of file_list) {
			let filePromise = new Promise(resolve => {
				let reader = new FileReader();
				reader.readAsText(file);
				reader.onload = () => resolve(new Array(file.name,reader.result));
			});
			

			promises.push(filePromise);


		}
		
		
		Promise.all(promises).then(fileContents => {
			console.log("fini");
			


			//console.log(fileContents[0][0]);
			//console.log(fileContents[1][1]);
			// fileContents will be an array containing
			// the contents of the files, perform the
			// character replacements and other transformations
			// here as needed

			//Pivot
			
			why.processfiles(fileContents);

			
		});
	}

	processfiles(fileContents){

		let why =this;
		for (let tab of fileContents) {
			let filename = tab[0];
			let ext = filename.split(".")[1];
			if(ext == "ptt"){
				//filesPtt.push(files[i]);
				// parse ptt 
				console.log(filename);	
				why.previewFilePtt(tab);		
			}
		}
		this.stopstandby();
		//SNP
		
		for (let tab of fileContents) {
			let filename = tab[0];

			let ext = filename.split(".")[1];
			if(ext == "snp"){
				console.log(filename);
				why.previewFileSNPRN(tab);		
			}
			if(ext == "del"){
				console.log(filename);
				//why.previewFileDel(tab);		
			}
			if(ext == "fasta"){
				console.log(filename);	
				why._fastaLoader = new FastaLoader(tab[0]);
				why._fastaLoader.read(tab[1]);	
			}
			

		}
		for (let tab of fileContents) {
			let filename = tab[0];
			let ext = filename.split(".")[1];
			if(ext == "nw"){
				console.log(filename);
				why._treeLoader = new TreeLoader(tab[0],this);
				why._treeLoader.read(tab[1]);		
			}
			if(ext == "grp"){
				console.log(filename);
				this._grpLoader = new TabulatedGroupLoader(tab[0],this);
				this._grpLoader.parseGroupFile(tab[1]);	
			}			
		}

		why.parent.doRedraw();
		why.parent.initSlider();


		console.log("DoDraw");
		this.parent.doDraw();

		var dictStrains;

		if (this._strainManager != null){
			dictStrains = this._strainManager._dictStrains;
		}
		console.log(dictStrains);

		
		if (this._treeLoader!=null){
			var tree = this._treeLoader._tree;
			for (let i = 0; i < tree.leaves.length; i++) {


				if(tree.leaves[i].id =="ref"){
					tree.leaves[i].data = {
						cell: {
						colour: '#FFFFFF',
						
						},
						passage:  {
							colour:'#FFFFFF',
							
						},
						tp:  {
							colour: '#FFFFFF',
							
						},
						type:  {
							colour: '#FFFFFF',
							
						},
					};
				}
				else{
					let dictTypeStrain = dictStrains[tree.leaves[i].id];

					tree.leaves[i].data = dictStrains[tree.leaves[i].id];
					//console.log(tree.leaves[i].id);
					//console.log(tree.leaves[i].data);
				}
				/*tree.leaves[i].data = {
				column1: {
					colour: '#3C7383',
					label: 'Label' + (i + 1),
				},
				column2: '#9BB7BF',
				column3: '#3C7383',
				column4: '#9BB7BF',
				};*/
			}
			
			// disable drag/drop also for tree
			

			
			this.parent._stageNames.destroy();
			tree.draw();
		}
		else{
			//this.parent._layerSnp.draw();
        	//this.parent._layerSnp.batchDraw();
			console.log(this.parent._layerNames);
        	this.parent._layerNames.draw();
			console.log("add strainNames");
		}

		
	}

	previewFilePtt(tab){

		var pttL = new PttLoader(tab[0],this);
		pttL.parsePttFile(tab[1]);
		console.log("canvasRef "+this.canvasRef+" "+this.mapLength);
		this.mapRef = new GenomeMap(this.canvasRef,this.mapLength,this.mapHeight,this.mapWidth,this.repliconL,'ref',this);
			
		this.mapRef.genes = pttL.genes;
		this.mapRef.debut =1;
		this.mapRef.fin =1+ this.mapRef.mapLength;
		console.log(this.mapRef.debut +" "+this.mapRef.fin);
		this.mapRef.computeMap();
		
		//this.parent.doDraw();

	}
	previewFileSNPRN(tab){
		this.strainsNumber++;
		console.log(this.mapLength);
		var snpsmap = new SnpMap(this.canvasSNP,this.mapLength,this.mapHeight,this.mapWidth,this.repliconL,tab[0],this,this.strainsNumber);
		    
	    this.mapsSnp[tab[0]] = snpsmap;
		var snpL = new SnpLoader(tab[0],snpsmap,this);
		snpL.loadSnps(tab[1]);

	}

	async handleFiles(files) {
		//console.log("Pre " +files);
		var files = [...files];
		//console.log("After " +files);
		//files.forEach(this.previewFile);

		var filesSnp = new Array();
		var fileSeq;
		var fileRef;
		var fileGrp;
		var fileNw;

		var filesDel=new Array();


		for(var i=0; i<files.length; i++){
			let ext = files[i].name.split(".")[1];	
			//if(ext == "csv"){
			if(ext == "ptt"){
				fileRef = files[i];
			}
			else if (ext == "snp"){
				//console.log(files[i].name);
				filesSnp.push(files[i]);
			}
			else if ((ext == "fasta")||(ext== "fa")){
				console.log("fasta "+files[i].name);
				fileSeq = files[i];
			}
			else if (ext == "grp"){
				console.log("grp "+files[i].name);
				fileGrp = files[i];
			}
			else if (ext == "nw"){
				console.log("nw "+files[i].name);
				fileNw = files[i];
			}
			/*else if (ext == "del"){
				console.log("tsv "+files[i].name);
				filesDel.push(files[i]);
			}*/
		}

		let reader = new FileReader()
		reader.readAsText(fileRef);

		var why = this;
		console.log(why);

		reader.onloadend = async function() {

			console.log("Read TSV "+fileRef.name)
			var pttL = new PttLoader(fileRef.name,why);
			pttL.parsePttFile(reader.result);

			console.log("canvasRef "+why.canvasRef);
			why.mapRef = new GenomeMap(why.canvasRef,why.mapLength,why.mapHeight,why.mapWidth,why.repliconL,'ref',why);
			//this.mapRef.createScale(1);

			


			why.mapRef.genes = pttL.genes;
			why.mapRef.debut =1;
			why.mapRef.fin =1+ why.mapRef.mapLength;
			console.log(why.mapRef.debut +" "+why.mapRef.fin);
			why.mapRef.computeMap();

			console.log("DoDraw");
			why.parent.doDraw();

			//console.log(filesDel);

			filesSnp.forEach( await why.previewFileSNP.bind(why));
			filesDel.forEach( await why.previewFileDel.bind(why));
			why._fastaLoader = new FastaLoader(fileSeq);
			why._fastaLoader.load();

			why._treeLoader = new TreeLoader(fileNw,why);
			why._treeLoader.load();

			why._grpLoader = new TabulatedGroupLoader(fileGrp,why);
			why._grpLoader.load();

		}

		// TO Modyfy : --> await  why._fastaLoader
		await why.resolveAfter1Second();
		
		
		//console.log(this);
		console.log(this._treeLoader._tree);
		var tree = this._treeLoader._tree;
		var dictStrains = this._strainManager._dictStrains;
		console.log(dictStrains);
		for (let i = 0; i < tree.leaves.length; i++) {


			if(tree.leaves[i].id =="ref"){
				tree.leaves[i].data = {
					cell: {
					  colour: '#FFFFFF',
					  
					},
					passage:  {
						colour:'#FFFFFF',
						
					  },
					tp:  {
						colour: '#FFFFFF',
						
					  },
					type:  {
						colour: '#FFFFFF',
						
					  },
				  };
			}
			else{
				let dictTypeStrain = dictStrains[tree.leaves[i].id];

				tree.leaves[i].data = dictStrains[tree.leaves[i].id];
				console.log(tree.leaves[i].id);
				console.log(tree.leaves[i].data);
		    }
			/*tree.leaves[i].data = {
			  column1: {
				colour: '#3C7383',
				label: 'Label' + (i + 1),
			  },
			  column2: '#9BB7BF',
			  column3: '#3C7383',
			  column4: '#9BB7BF',
			};*/
		}
		tree.draw();

		why.parent.doRedraw();
		why.parent.initSlider();
	  //tabulate(files);
	}
	resolveAfter1Second() {
		return new Promise(resolve => {
		  setTimeout(() => {
			resolve('resolved');
		  }, 1000);
		});
	}  
	 
	  


	createRef(ref){
		console.log("createRef");
		this.mapRef = new GenomeMap(this.canvasRef,this.mapLength,this.mapHeight,this.mapWidth,this.repliconL,'ref',this);
		//this.mapRef.createScale(1);

		var why =this;

		var genes= [];
		//console.log(this);
		ref.forEach(function(d) {
			//console.log(genes);
			//console.log(why);
			//console.log(d);
			var gene =new Gene(d.Synonym,Number.parseInt(why.parse(d)),Number.parseInt(why.parseTo(d)),d.Strand);
			genes.push(gene);
			why.hashNameGenes[d.Synonym]= gene;
		});
		this.mapRef.genes = genes;
		this.mapRef.debut =1;
		this.mapRef.fin =1+ this.mapRef.mapLength;
		this.mapRef.computeMap();


	}


	previewFileSNP(file) {


		//console.log(this);
		//console.log("!!!!! previewFileSNP");
		this.strainsNumber++;
		let reader = new FileReader();
		//reader.readAsDataURL(file)
		//console.log(file.name);
		reader.readAsText(file)

		//console.log(file.name+" "+this.strainsNumber*10);
		var simpleText = new Konva.Text({
						      x: 0,
						      y: this.strainsNumber*10,
						      text: file.name,
						      fontSize: 8,
						      fill: 'black'
							});
		//console.log(this.parent._layerNames);

		/*var circle = new Konva.Circle({
            x: 20,
            y: 20,
            radius: 70,
            fill: 'red',
            stroke: 'black',
            strokeWidth: 2
        });
        // add the shape to the layer
        this.parent._layerNames.add(circle);*/


		//this.parent._layerNames.add(simpleText);

		//console.log(this);
		//console.log(this.canvasSNP);
		//console.log(this.parent);
		//console.log(this.parent._canvas);
		//console.log(this.canvasRef);
		//console.log(this._canvasRef);
		//console.log(this.parent._canvasSNP);

		//console.log(file.name+" "+this.strainsNumber*10);
		//console.log("strainNumber "+this.strainsNumber);
		var snpsmap = new SnpMap(this.canvasSNP,this.mapLength,this.mapHeight,this.mapWidth,this.repliconL,file.name,this,this.strainsNumber);
		     
		   //snpsmap.createScale(1);
	    this.mapsSnp[file.name] = snpsmap;
	    var why = this;

		reader.onloadend = function() {
			
			   //	console.log("read Snp "+file.name);
			   //	console.log(snpsmap);
			   	// add snp file in SNParray 
			   	var snpL = new SnpLoader(file.name,snpsmap,why);
			   	snpL.loadSnps(reader.result);
			
		} 
			    
	}

	
	previewFileDel(file) {


		console.log(this);
		console.log("!!!!! previewFileSNP");
		this.strainsNumber++;
		let reader = new FileReader();
		//reader.readAsDataURL(file)
		console.log(file.name);
		reader.readAsText(file)

		console.log(file.name+" "+this.strainsNumber*10);
		var simpleText = new Konva.Text({
						      x: 0,
						      y: this.strainsNumber*10,
						      text: file.name,
						      fontSize: 8,
						      fill: 'black'
							});
		var delmap = new DelMap(this.canvasSNP,this.mapLength,this.mapHeight,this.mapWidth,this.repliconL,file.name,this,this.strainsNumber);
		     
		   //snpsmap.createScale(1);
	    this.mapsSnp[file.name] = delmap;
	    var why = this;

		reader.onloadend = function() {
			
			   //	console.log("read Snp "+file.name);
			   //	console.log(snpsmap);
			   	// add snp file in SNParray 
			   	var snpL = new DelLoader(file.name,delmap,why);
			   	snpL.loadDels(reader.result);
			
		} 
			    
	}

	async standbyAnimation(){	
		var htmlCanvas = document.getElementById('canvas');
		console.log("canvas");

		var htmlCanvasjq = $('canvas');
		console.log(htmlCanvasjq);
		htmlCanvas.width = 1500;
		htmlCanvas.height = 800;


		let w;
		let offscreen;
		var hasOffscreenSupport = !!htmlCanvas.transferControlToOffscreen;
		if (hasOffscreenSupport) {
			offscreen = htmlCanvas.transferControlToOffscreen();
			this._worker = new Worker('./workerStandBy.js');
			await this._worker.postMessage({ canvas: offscreen }, [offscreen]);
			// wait for animation
			//load all the files
			this.handleFilesRN(this._files);


		}
		else {
			/*htmlCanvas
			.getContext('2d')
			.fillText(
				'🛑 Sorry, your browser does not support Offscreen rendering...',
				20,
				20
			);*/
			this.handleFilesRN(this._files);
		}
	}
	stopstandby(){
		if(this._worker!=null){
			this._worker.terminate();
		}
		
		var htmlCanvas = document.getElementById('canvas');
		htmlCanvas.remove();
		//$('canvas').remove();
	}


}


export default MapManager;
