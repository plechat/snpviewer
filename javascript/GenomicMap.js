class GenomicMap{

	constructor(canvas,mapL,mapH,mapW,repliconL,title,manager){


		this.parentCanvas=canvas;
		this.offsetY =0;
		this.offsetGlobalY = 0;
		this.offsetX ;
		this.mapLength=mapL,  // local map size
		this.mapHeight=mapH,
		this.mapWidth=mapW,

		this.title = title;		
		this.label='',
		
		
		this.debut=1;
		this.fin=mapL;


		//public var replicon:Replicon;
		this.repliconlength=repliconL;
		
		// number of the map for best hit
		this.rank=1;
		
		this.conv=this.mapWidth/this.mapLength;

		this.manager = manager;
		//public var spMap:UIComponent;
	}
	prev(){}
	next(){}
	full(){}
	erase(){}

	/*get debut(){
		return this.debut;
	}

	set debut(d){
		this.debut=d;
	}
	get fin(){
		return this.fin;
	}

	set fin(f){
		this.fin=f;
	}*/


	computeMapWithCoord(d,e,isGraduate){}
	//computeMap:function(){},
	prev(){}

	isOnMapBegin(){
			 	
			if (this.fin -this.mapLength <=0){
				return true;
			}
			else return false;
	}
	isOnMapEnd(){
	 	//trace (debut +" " +mapLength+" "+replicon.length);
		//if (this.debut +this.mapLength >= replicon.length){
		if (this.debut +this.mapLength >= repliconlength){
		
			return true;
		}
		else return false;
	}
			
	redraw() {
				
	}
	 	

	createScale(isgraduate,height){
		
	}

	createScaleOld(isgraduate){
		
		   	//var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		    
		    var svgNS = this.parentSVG.namespaceURI;


		    //var xmlns = "http://www.w3.org/2000/svg";
		    //svg.setAttribute('width',this.convert(this.mapLength));
		    //svg.setAttribute('height',this.mapHeight);
		    svg.setAttribute('width','100%');

		    var inter = this.mapLength/10;
		    //console.log("inter "+inter+" "+this.mapLength);
		    for (var i=0;i<10;i++){
		    	if (i/2 -Math.round(i/2) ==0){
		    		var rect = document.createElementNS(svgNS,'rect');
		    		rect.setAttribute('fill','#eceff4');
		    		rect.setAttribute('x',this.convert(i*inter));
		    		//rect.setAttribute('y',this.rank*(this.mapHeight-1));
		    		rect.setAttribute('y',0);
		    		
		    		rect.setAttribute('width',this.convert(inter));
		    		rect.setAttribute('height',this.mapHeight);

		    		//console.log(parent);
		    		this.parentSVG.appendChild(rect);
		    	}

		    }
		  	//document.body.appendChild(svg);
	}
	convert(x)
	{
		    
		    var num = x *this.conv;
		    return num;
		       			
	}
	computeMap(){
	}
	
}

export default GenomicMap;