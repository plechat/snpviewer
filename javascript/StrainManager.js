import StrainGroup from "./StrainGroup";

class StrainManager{

    // Collection of strains 
    // type -> value -> [strain]
    // temp -> 28 -> [id1,id2,id3,...]
	constructor(){
		
        this._dict= {}; //type ->dict[value]->StrainGroup ->tabStrains
        this._dictStrains ={}; // strains->type->dict[value]->Strain
		
	}
	
	addGroup(type){
        //this._strain.push(id);
        this._dict[type];
	}
    addStrainGroupType(type){
        
        this._dict[type]={}; // ->value
    }
    addStrainGroup(type,value,id){
        //console.log(type+" "+value);
        var dictValue = this._dict[type];
        var stg;
        if(value  in dictValue === false){
            stg = new StrainGroup(value);

            dictValue[value]= stg;
            stg.addStrain(id);
            this._dict[type] = dictValue;
        }
        else{
            stg = dictValue[value];
            stg.addStrain(id);
        }

        //strain id -> type-> straingrp
        if(id in this._dictStrains === false){
            var dict ={};
            var dictTmp ={};
            dictTmp["colour"]= stg.color;
            dict[type]= dictTmp;
            this._dictStrains[id]= dict;

        }
        else{

            var dictTypeOfStrain = this._dictStrains[id];
            var dictTmp ={};
            dictTmp["colour"]= stg.color;
            dictTypeOfStrain[type]= dictTmp;
        }
        

    }
    /*getDictStrain(){

        for (var type in this._dict) {
            straingroup = 
        }

    }*/

    getStrainsByType(type){
        var tab = this._dict[type];
        var tabStrains= new Array();

        for (var straingroup in tab) {
            console.log(straingroup._tabStrain);
            tabStrains = tabStrains.concat(straingroup._tabStrain);

        }
        
        return tabStrains;
    }

    getTypeColorBystrain(strainId){
        console.log(strainId)
    }



	get color(){
		return this._color;
	}
	get dict(){
		return this._dict;
	}
	
	

}


export default StrainManager;