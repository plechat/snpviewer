class StrainGroup{

	// Collection of strains 
	constructor(val){
		this._val=val;
		this._tabStrain = new Array();
		this._color = this.getRandomColor();
		//this._toshow =true;
	}
	
	addStrain(id){
		this._tabStrain.push(id);
	}
	get label(){
		return "";
	}
	
	get color(){
		return this._color;
	}
	get val(){
		return this._val;
	}
	
	//get toShow(){
	//	return this._toshow;
	//}
	getRandomColor() {
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
		  color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
}


export default StrainGroup;