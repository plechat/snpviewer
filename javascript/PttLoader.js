import MapManager from "./MapManager";
import Organism from "./Organism";
import Replicon from "./Replicon";

import Gene from "./Gene";


import 'babel-polyfill';

class PttLoader{
	constructor(fileName,manager){
		//this._file = file;

		//this._Organism = "";
		//this._data =null;
		this.genes = [];
		this.manager= manager;
		this.end;
	}
	

	async read(){
		var content = await this.load(this._file);		
		var data = content.split(/\n\r?/gi);

    	//while (data.length && data[0][0] === '>') {

    	//	console.log();
      	//	data.shift();
    	//}
    	//this._seq = data.join('');
    	for (var i=0;i<data.length;i++){
			
    		console.log(data[i]);
		}   	

	}

	parsePttFile(textIn){

		var org = new Organism();
		var rep = new Replicon("test");
		//console.log(this.data);


		
			/*ref.forEach(function(d) {
				var gene =new Gene(d.Synonym,Number.parseInt(why.parse(d)),Number.parseInt(why.parseTo(d)),d.Strand);
				genes.push(gene);
				why.hashNameGenes[d.Synonym]= gene;
			});*/

		var lines = textIn.split("\n");

		var numLines = lines.length-1;
		console.log(numLines);

		//Location        Strand  Length  PID     Gene    Synonym Code    COG     Product

		console.log(this.manager);
		var tabs;

		let tabOrg =lines[0].split("\,");
		let tmp =	tabOrg[1].split("\.");
		console.log(tabOrg[0]+" "+tmp[2]);
		this.manager.mapLength = Number.parseInt(tmp[2]);
		this.manager.parent.setMapLength();
		
		for (var i=3; i<numLines; i++) {

			//console.log(lines[0]);
			tabs = lines[i].split("\t");

			console.log("id " +tabs[5]);
			var gene =new Gene(tabs[5],Number.parseInt(this.parse(tabs[0])),Number.parseInt(this.parseTo(tabs[0])),tabs[1]);
			//console.log(gene.fromAbs+" "+gene.toAbs);

			this.manager.hashNameGenes[tabs[5]]= gene;
			this.genes.push(gene);


		}

		



		

	}

	parseLength(e){
    var from = (e.split(/\.\./))[0];
    var to = (e.split(/\.\./))[1];
	//e.from = 
		return to -from +1;
	}
	parse(e){
	    var from = (e.split(/\.\./))[0];
		//e.from = 
		return from;
	}
	parseTo(e){
	    var from = (e.split(/\.\./))[1];
		//e.from = 
		return from;
	}
	strand(e){
	    if (e.Strand == '+'){
	    	return 0;
	    }
	    else{
	    	return 50;
	    }
	}
	type(d) {
	  d.value = +d.value; // coerce to number
	  return d;
	}

	load(url) {
	  var why =this;
	  return new Promise(function(resolve, reject) {
	    const xhr = new XMLHttpRequest();
	    xhr.onreadystatechange = function(e) {
	      if (xhr.readyState === 4) {
	        if (xhr.status === 200) {
	          //resolve(url+"\n"+xhr.response);
	          why.data = xhr.response;
	          resolve(why);
	        } else {
	          reject(xhr.status);
	        }
	      }
	    }
	    xhr.ontimeout = function () {
	      reject('timeout');
	    }
	    xhr.open('get', url, true);
	    xhr.send();

	    
	  })
	}
	parseFrom(e){
	    var from = (e.split(/\.\./))[0];
		//e.from = 
		return from;
	}
	parseTo(e){
	    var to = (e.split(/\.\./))[1];
		//e.from = 
		return to;
	}



	
}



export default PttLoader;