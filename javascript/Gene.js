
import GenomicObject from "./GenomicObject";

class Gene extends GenomicObject{

	constructor(id,begin,end,direction){
		super(begin,end,direction);
		this._id=id;
		this._color="";
		//this._fromAbs=begin;
		//this._toAbs =end;
		//this._direction = direction;
	}
	get length(){
		return this._toAbs-this._fromAbs+1;
	}
	
	get id(){
		return this._id;
	}
	get color(){
		return this._color;
	}
	set color(col){
		this._color=col;
	}	

}
export default Gene;