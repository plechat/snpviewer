
const HEX = 1;
const RGB = 2;
const RGBA = 3;

class ColorGradient{
	constructor(size,color1,color2,type){
	
		this._steps = size;
	    this._val1El = new Color(color1,type);
		this._val2El = new Color(color2,type);


		// constants for switch/case checking representation type
		this._HEX = 1;
		this._RGB = 2;
		this._RGBA = 3;
	}

	get steps(){
		return this._steps;
	}

	get val1El(){
		return this._val1El;
	}
	get val2El(){
		return this._val2El;
	}



	// get the string representation 
	// type and set it on the element (HEX/RGB/RGBA)
	getType(val) {
	  if (val.value.indexOf('#') > -1) return HEX;
	  if (val.value.indexOf('rgb(') > -1) return RGB;
	  if (val.value.indexOf('rgba(') > -1) return RGBA;
	}

	// process the value irrespective of representation type
	processValue(el) {
	  //console.log(el.dataType);
	  switch (el.dataType) {
	    case HEX:
	      {
	        return this.processHEX(el.value);
	      }
	    case RGB:{
	      return this.processRGB(el.value);
	    }
	    case RGBA:{
	      return this.processRGB(el.value);
	    }
	      
	  }
	}

	//return a workable RGB int array [r,g,b] from rgb/rgba representation
	processRGB(val){
	  var rgb = val.split('(')[1].split(')')[0].split(',');
	  alert(rgb.toString());
	  return [
	      parseInt(rgb[0],10),
	      parseInt(rgb[1],10),
	      parseInt(rgb[2],10)
	  ];
	}

	//return a workable RGB int array [r,g,b] from hex representation
	processHEX(val) {
	  //console.log("processHEX");
	  //does the hex contain extra char?
	  var hex = (val.length >6)?val.substr(1, val.length - 1):val;
	  // is it a six character hex?
	  if (hex.length > 3) {

	    //scrape out the numerics
	    var r = hex.substr(0, 2);
	    var g = hex.substr(2, 2);
	    var b = hex.substr(4, 2);

	    // if not six character hex,
	    // then work as if its a three character hex
	  } else {

	    // just concat the pieces with themselves
	    var r = hex.substr(0, 1) + hex.substr(0, 1);
	    var g = hex.substr(1, 1) + hex.substr(1, 1);
	    var b = hex.substr(2, 1) + hex.substr(2, 1);

	  }
	  // return our clean values
	    return [
	      parseInt(r, 16),
	      parseInt(g, 16),
	      parseInt(b, 16)
	    ]
	}

	updateSpitter() {
	    //attach start value
	    var hasSpun = 0;
	    //console.log(this._val1El);

	    //console.log("type "+this.getType(this.val1El));

	    //this._val1El.dataType = this.getType(this._val1El);
	    //this._val2El.dataType = this.getType(this._val2El);


	    //console.log(this.getType(this._val1El));
	    //console.log(this._val1El);
	    //console.log("type "+this._val1El._dataType);
	    //console.log("type "+this.val1El._dataType);

	    //console.log("HEx "+ HEX);

	    var val1RGB = this.processValue(this.val1El);
	    var val2RGB = this.processValue(this.val2El);
	    //console.log("val " +val1RGB[0]);
	    var colors = [
	      // somewhere to dump gradient
	    ];
	    // the pre element where we spit array to user
	    //var spitter = document.getElementById('spitter');

	    //the number of steps in the gradient
	    //var stepsInt = parseInt(this.steps.value, 10);
	    var stepsInt = this.steps;
	    //the percentage representation of the step
	    var stepsPerc = 100 / (stepsInt+1);

	    // diffs between two values 
	    var valClampRGB = [
	      val2RGB[0] - val1RGB[0],
	      val2RGB[1] - val1RGB[1],
	      val2RGB[2] - val1RGB[2]
	    ];
	  
	    // build the color array out with color steps
	    for (var i = 0; i < stepsInt; i++) {
	      var clampedR = (valClampRGB[0] > 0) 
	      ? this.pad((Math.round(valClampRGB[0] / 100 * (stepsPerc * (i + 1)))).toString(16), 2) 
	      : this.pad((Math.round((val1RGB[0] + (valClampRGB[0]) / 100 * (stepsPerc * (i + 1))))).toString(16), 2);
	      
	      var clampedG = (valClampRGB[1] > 0) 
	      ? this.pad((Math.round(valClampRGB[1] / 100 * (stepsPerc * (i + 1)))).toString(16), 2) 
	      : this.pad((Math.round((val1RGB[1] + (valClampRGB[1]) / 100 * (stepsPerc * (i + 1))))).toString(16), 2);
	      
	      var clampedB = (valClampRGB[2] > 0) 
	      ? this.pad((Math.round(valClampRGB[2] / 100 * (stepsPerc * (i + 1)))).toString(16), 2) 
	      : this.pad((Math.round((val1RGB[2] + (valClampRGB[2]) / 100 * (stepsPerc * (i + 1))))).toString(16), 2);
	      colors[i] = [
	        '#',
	        clampedR,
	        clampedG,
	        clampedB
	      ].join('');
	    }
	    return colors;
	  //build div representation of gradient
	  /*var html = [];
	    for(var i = 0;i<colors.length;i++){
	      html.push("<div class='color' style='background-color:"+colors[i]+"; height:"+((i-(i-1))/colors.length*100)+"%;'></div>");
	    }
	 document.getElementById("colors").innerHTML = html.join('');
	    //update the pre element
	    spitter.innerText = JSON.stringify(colors);*/
	}
  /**
   * padding function:
   * cba to roll my own, thanks Pointy!
   * ==================================
   * source: http://stackoverflow.com/questions/10073699/pad-a-number-with-leading-zeros-in-javascript
   */
	pad(n, width, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	}

}
class Color {

	constructor(value,type){
		
		this._dataType=type;
		this._value =value;
		//this._direction = direction;
	}
	get dataType(){
		return this._dataType;
	}

	set dataType(type){
		this._dataType=type;
	}
	
	get value(){
		return this._value;
	}
	
}

export default ColorGradient;
