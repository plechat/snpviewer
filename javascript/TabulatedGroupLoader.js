import StrainManager from "./StrainManager";

class TabulatedGroupLoader{
	constructor(file,manager){
		this._file = file;

		this._manager = manager;
		
	}
	

	load() {

		let reader = new FileReader();
		reader.readAsText(this._file);
		let why =this;
		reader.onloadend = function() {
			why.parseGroupFile(reader.result);
		}
	}



	

	parseGroupFile(textIn){

		//console.log(this.data);


		
			/*ref.forEach(function(d) {
				var gene =new Gene(d.Synonym,Number.parseInt(why.parse(d)),Number.parseInt(why.parseTo(d)),d.Strand);
				genes.push(gene);
				why.hashNameGenes[d.Synonym]= gene;
			});*/

		var strainManager = new StrainManager();
		
		//console.log(this._manager.strainManager);
		var lines = textIn.split("\n");

		var numLines = lines.length-1;
		//console.log(numLines);

		//strain	temp type
		// id type1 type2 type3
		var tabs  = lines[0].split("\t");
		var dictType = new Array();
		//console.log("ICI "+lines[0]+" ... "+tabs);
		for (var j=1; j<tabs.length; j++) {

			//console.log("Loader add type "+tabs[j]);
			strainManager.addStrainGroupType(tabs[j]);
			dictType.push(tabs[j]);
		}


		for (var i=1; i<numLines; i++) {

			//console.log(lines[0]);
			tabs = lines[i].split("\t");
			//console.log(tabs[0]);
			for (var j=1; j<tabs.length; j++) {
				let type = dictType[j-1];
				//if( type in strainManager._dict === false){
				strainManager.addStrainGroup(type,tabs[j],tabs[0]);
				

			}
			
			//console.log("id " +tabs[0]+" "+tabs[1]);
			//var gene =new Gene(tabs[5],Number.parseInt(this.parse(tabs[0])),Number.parseInt(this.parseTo(tabs[0])),tabs[1]);
			//console.log(gene.fromAbs+" "+gene.toAbs);

			


		}
		this._manager.strainManager =strainManager;
		

	}

	

	
}



export default TabulatedGroupLoader;