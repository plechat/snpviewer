import Snp from "./Snp";

class SnpLoader{
	
	constructor(strainName,snpMap,manager){
		this._data =null;

		this._strainName = strainName;
		this._snpsmap = snpMap;
		this.manager =manager;
	}
	

	get data(){
		return this._data;
	}
	/*async read(){
		var content = await this.load(this._file);		
		var data = content.split(/\n\r?/gi);

    	//while (data.length && data[0][0] === '>') {

    	//	console.log();
      	//	data.shift();
    	//}
    	//this._seq = data.join('');
    	for (var i=0;i<data.length;i++){
			
    		console.log(data[i]);
		}
    	
	}*/

	parseSNPfile(){

		console.log("parseSNPfile");

		//console.log(this.data);
		var org = new Organism("test");


		return org;

	}

	loadSnps(textIn,snpM){


		var snpsmap = snpM;
		var lines = textIn.split("\n");

		var numLines = lines.length-1;
		//console.log(numLines);

		var manager = this.manager;

		var dictBase = {
			  A: 0,
			  T: 1,
			  G: 2,
			  C: 3
			};

		// position	reference	alternate	coverage	alt_frequency	gene	aa	aamut
		// 19	C	A	5	0.4	
		// 77	A	G	16774	0.0004769285799451532	NSP1	M	V

		for (var i=1; i<numLines; i++) {

			//console.log(lines[0]);
			var tabs = lines[i].split("\t");

			var alter =tabs[2];    //alternate
            var colorSnp;
            if(alter == "T"){
        		colorSnp ="red";
	        }
	        else if(alter == "C"){
	        	colorSnp ="blue";
	        }
	        else if(alter == "A"){
	        	colorSnp ="green";
	        }
	        //else if(alter == "G"){
	        else {	
	        	colorSnp ="orange";
	        }

	        let pos = tabs[0];
	        let ref = tabs[1];
			let cover = tabs[3];
			let freq = tabs[4];
			let gene = tabs[5];
			let codonref =  tabs[6];
			let codonmut =  tabs[7];

			



	        //(id,begin,end,direction,gene,freq,coverage,reference,alternate,colorSNP,aa,aamut)
	        var snp = new Snp('',pos,'','',gene,freq,cover,ref,alter,colorSnp,codonref,codonmut);
		

            this._snpsmap._snps.push(snp); // ...

            var heatkey = pos+alter;
           	if(pos in manager.posSnpDict){
           		var tab =manager.posSnpDict[pos];
           		var indice = dictBase[alter];
           		tab[indice]=tab[indice]+1;
           		manager.posSnpDict[pos]=tab;
           		
           	}
           	else{
           		var tab = [0,0,0,0];
           		tab[dictBase[alter]]= 1;
           		manager.posSnpDict[pos]=tab;
           	}

			// dict to export seq alignment
			
           	if (pos in manager.snpArtificialSeq){
				var snpAtPos = manager.snpArtificialSeq[pos];
				snpAtPos[this._strainName] = snp ;
			}
			else{
				var AtPos= {};
				AtPos[this._strainName] = snp ;
				AtPos["ref"]=ref;
				manager.snpArtificialSeq[pos] = AtPos;
			}
			
			
			// heatmap 

           	if (heatkey in manager.heatmapSnpDict){
           		manager.heatmapSnpDict[heatkey]++; 
           	}
           	else{
           		manager.heatmapSnpDict[heatkey]=1;
           	} 
			
		}

		//return new Promise(resolve => true);

	}



	async load(url) {

	  	var why =this;
	  	return new Promise(function(resolve, reject) {
		    const xhr = new XMLHttpRequest();
		    xhr.onreadystatechange = function(e) {
		      if (xhr.readyState === 4) {
		        if (xhr.status === 200) {
		          why._data = xhr.response;
		          resolve(why);

		        } else {
		          reject(xhr.status);
		        }
		      }
		    }
		    xhr.ontimeout = function () {
		      reject('timeout');
		    }
		    xhr.open('get', url, true);
		    xhr.send();

		    
		});
	}

	
}

export default SnpLoader;