class FastaLoader{

	constructor(file){
		this._file = file;
		this._header = "";
		this._seq = "";
	}
	

	load() {

		let reader = new FileReader();
		reader.readAsText(this._file);
		let why =this;
		reader.onloadend = function() {
			why.read(reader.result);
		}
	}

	read(seq){
		console.log(this._file);
		this._seq =seq;
		


		var data = this._seq.split(/\n\r?/gi);

		
    	while (data.length && data[0][0] === '>') {
      		data.shift();
    	}

    	//console.log(data.join(''));
    	this._seq = data.join('');
    	//console.log(this.getSubSeq(0,1));
    	//console.log(this.getSubSeq(0,15));
    	//console.log(this.getSubSeq(5533,5536));
    	//console.log("l "+this._seq.length);

	}

	
	

	/*load(url) {
	  return new Promise(function(resolve, reject) {
	    const xhr = new XMLHttpRequest();
	    xhr.onreadystatechange = function(e) {
	      if (xhr.readyState === 4) {
	        if (xhr.status === 200) {
	          resolve(xhr.response);
	        } else {
	          reject(xhr.status);
	        }
	      }
	    }
	    xhr.ontimeout = function () {
	      reject('timeout');
	    }
	    xhr.open('get', url, true);
	    xhr.send();

	    
	  })
	}*/



	getSubSeq(begin,end){
		let subSeq =this._seq.substring(begin, end);

		return subSeq;

	}
	getSeqAround(begin,nuc){
		let posB =begin-12;
		let posE =begin-1;
		console.log(posB+" "+posE);
		let subSeq =this._seq.substring(posB, posE);
		posB =begin+1;
		posE =begin+12;
		console.log(posB+" "+posE);
		let subSeq2 =this._seq.substring(posB, posE);
		console.log(begin);
		console.log(subSeq);
		console.log(subSeq2);
		let seq = subSeq+nuc+subSeq2;

		return seq;
	}

}

export default FastaLoader;