
import { generateAsync } from 'jszip';
import 'regenerator-runtime/runtime';
// load konva framework
importScripts('https://unpkg.com/konva@4.2.0/konva.min.js');


Konva.Util.createCanvasElement = () => {
    const canvas = new OffscreenCanvas(1, 1);
    canvas.style = {};
    return canvas;
};
  
// now we can create our canvas content
var stage = new Konva.Stage({
width: 200,
height: 200
});

var layer = new Konva.Layer();
stage.add(layer);

layer.draw();

onmessage = function(evt) {
    console.log('Worker: Message received from main script');
    console.log(evt);
    console.log(evt.data['canvas']);
    //const result = e.data[0] * e.data[1];
    /*if (isNaN(result)) {
      postMessage('Please write two numbers');
    } else {
      const workerResult = 'Result: ' + result;
      console.log('Worker: Posting message back to main script');
      postMessage(workerResult);
    }*/
    if (evt.data.canvas) {
        var canvas = evt.data.canvas;
        // adapt stage size
        // we may need to add extra event to resize stage on a fly
        stage.setSize({
          width: canvas.width,
          height: canvas.height
        });
        const ctx = canvas.getContext('2d');
        // Konva.Layer has support for "draw" event
        // so every time the layer is re-rendered we need to update the canvas
        layer.on('draw', () => {
            // clear content
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            // draw layer content
            ctx.drawImage(layer.getCanvas()._canvas, 0, 0);
        });
        //runBunnies();
    }
}


function requestAnimationFrame(cb) {
    setTimeout(cb, 16);
}
  
// that function is large and adapted from bunnies demo
// the only interesting part here is how to load images to use for Konva.Image
async function runBunnies() {
    let tab = ['S','Y','N','T','V','I','E','W','J','S'];
  let yvar =15;
  let tabText =[];
  let xvar;

  let tabcolor= [Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor()];
  let tabcolor2= [Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor(),Konva.Util.getRandomColor()];

  for (var i = 0; i < tab.length; i++) {

    if (i%2){
      xvar =100

    }
    else{
      xvar =150
    }
//Konva.Util.getRandomColor(),
    //let rwidth = 50+100* Math.random();
    

    let rwidth  = 100;
    var rect1 = new Konva.Rect({
      x: xvar-rwidth/2,
      y: yvar,
      width: rwidth,
      height: 15,
      fill: '#0ae973',
      stroke: 'black',
      opacity:0.8,
      strokeWidth: 1,
    });
    
    
    let group = new Konva.Group({
      x:0,
      y:0,
      }
    );

    

    var simpleText = new Konva.Text({
      x: xvar,
      y: yvar+1,
      text: tab[i],
      fontSize: 15,
      fontFamily: 'Calibri',
      fill: 'black',
    });
    /*let group = new Konva.Group({
      x:0,
      y:0,
      }
    );*/
    group.add(rect1)
    group.add(simpleText);

    layer.add(group);
    //layer.add(rect1);
    //layer.add(simpleText);

    let deb = xvar-rwidth/2;
    let tabshift =[60,20,10,40,50];
    
    //let shift=Math.random()*50;
    //let shift =tabshift[0];
    
    for (var j = 0; j < 5; j++) {
      
      deb -= tabshift[j]+10;
      //console.log(deb+" "+tabshift[j]);
      if(((i!=2)||(j!=3))&&((i!=4)||(j!=1))){
        var rect = new Konva.Rect({
          x: deb,
          y: yvar,
          width: tabshift[j],
          height: 15,
          fill: tabcolor[j],
          stroke: 'black',
          opacity:0.4,
          strokeWidth: 1,
        });
        group.add(rect);
      }
     
    }
    deb = rwidth+xvar-rwidth/2+10;
    //deb = xvar;
    
    let tabshift2 =[30,20,60,15,50];
    let shift=tabshift2[0];
    for (var j = 0; j < 5; j++) {
      
      if(((i!=4)||(j!=2))&&((i!=2)||(j!=5))){
        var rect = new Konva.Rect({
          x: deb,
          y: yvar,
          width: tabshift2[j],
          height: 15,
          fill: tabcolor2[j],
          stroke: 'black',
          opacity:0.4,
          strokeWidth: 1,
        });
        group.add(rect);
      }
      
      
      deb += tabshift2[j]+10;
    }

    tabText.push(group);
    yvar+=15;
  }
  

  var amplitude = 100;
  var period = 2000;
  // in ms
  var centerX = stage.width() / 2;



 
  var anim = new Konva.Animation(function (frame) {

    let xodd = amplitude * Math.sin((frame.time * 1 * Math.PI) / period) + 300;
    let xpair = -amplitude * Math.sin((frame.time * 1* Math.PI) / period)+350;


    let color;
    if ((xodd >= xpair-45)&&(xodd<xpair+45)){ 
    //if (Math.round(xodd) == Math.round(xpair)-2){ 
      xpair = xodd-50; // align the Letter
      color = 'red';
    }
     else {
      color = '#0ae973'
    }
    for (var i = 0; i < tab.length; i++) {
      if (i%2){
        tabText[i].x(
          xodd

        );
      }
      else{
        tabText[i].x(
          xpair
        );
        
      }
      tabText[i].getChildren(function(node){ return node.getClassName() === 'Rect'})[0].attrs["fill"] = color;
    }

  }, layer);

  anim.start();
}
  
runBunnies();