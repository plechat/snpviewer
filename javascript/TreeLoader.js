import Phylocanvas from 'phylocanvas';






class TreeLoader{

	constructor(file,man){
        this._file = file;
        this._manager = man;
        this._tree;
        this._offsetY;
        this._dictStrain; // strain -> leafid 
        this._root ;
        this._pixelratio;
	}
	

	load() {

    

		let reader = new FileReader();
		reader.readAsText(this._file);
		let why =this;
		reader.onloadend = function() {
			why.read(reader.result);
		}
	}

	read(treetxt){
        
        // phylocanvas tree
        

        this._tree = Phylocanvas.createTree('namesC',
        {
          metadata: {
            active: true,
            dragging:false,
            showHeaders: true,
            showLabels: false,
            blockLength: 32,
            blockSize: null,
            padding: 10,
            columns: [],
            propertyName: 'data',
            underlineHeaders: true,
            headerAngle: 90,
            lineWidth: .5,
            font: null,
          }
       });

        var tree = this._tree;
        // !!!! remove all event listener, even drag and drop
        tree.removeEventListeners();
        //var example_tree = "(((EELA:0.150276,CONGERA:0.213019):0.230956,(EELB:0.263487,CONGERB:0.202633):0.246917):0.094785,((CAVEFISH:0.451027,(GOLDFISH:0.340495,ZEBRAFISH:0.390163):0.220565):0.067778,((((((NSAM:0.008113,NARG:0.014065):0.052991,SPUN:0.061003,(SMIC:0.027806,SDIA:0.015298,SXAN:0.046873):0.046977):0.009822,(NAUR:0.081298,(SSPI:0.023876,STIE:0.013652):0.058179):0.091775):0.073346,(MVIO:0.012271,MBER:0.039798):0.178835):0.147992,((BFNKILLIFISH:0.317455,(ONIL:0.029217,XCAU:0.084388):0.201166):0.055908,THORNYHEAD:0.252481):0.061905):0.157214,LAMPFISH:0.717196,((SCABBARDA:0.189684,SCABBARDB:0.362015):0.282263,((VIPERFISH:0.318217,BLACKDRAGON:0.109912):0.123642,LOOSEJAW:0.397100):0.287152):0.140663):0.206729):0.222485,(COELACANTH:0.558103,((CLAWEDFROG:0.441842,SALAMANDER:0.299607):0.135307,((CHAMELEON:0.771665,((PIGEON:0.150909,CHICKEN:0.172733):0.082163,ZEBRAFINCH:0.099172):0.272338):0.014055,((BOVINE:0.167569,DOLPHIN:0.157450):0.104783,ELEPHANT:0.166557):0.367205):0.050892):0.114731):0.295021)";

        //var covtree ="((((P5_8_S56.snp:0.17301,P5_3_S51.snp:0.22646)0.945:0.06533,((P4_2_S38.snp:0.18758,(P5_6_S54.snp:0.12626,(P4_6_S42.snp:0.15135,P4_3_S39.snp:0.23150)0.850:0.06746)0.864:0.06005)0.299:0.02967,((((P5_2_S50.snp:0.17600,(P5_1_S49.snp:0.21786,P4_8_S44.snp:0.26847)0.964:0.06786)0.385:0.01165,P5_7_S55.snp:0.21237)0.956:0.05464,(P5_5_S53.snp:0.23417,P5_4_S52.snp:0.17562)0.394:0.04684)0.877:0.03389,((P4_4_S40.snp:0.17530,P2_9_S21.snp:0.26370)0.965:0.08168,((P4_10_S46.snp:0.24887,(P5_12_S60.snp:0.19063,P4_12_S48.snp:0.20015)0.199:0.05128)0.049:0.03908,(P4_7_S43.snp:0.13396,P4_5_S41.snp:0.21588)0.929:0.07575)0.387:0.01818)0.455:0.03289)0.782:0.03374)0.676:0.01268)0.549:0.02207,(((P3_2_S26.snp:0.20981,P3_7_S31.snp:0.21993)0.745:0.03455,((P3_12_S36.snp:0.13512,P2_5_S17.snp:0.16260)0.904:0.04401,(P3_6_S30.snp:0.20343,P2_6_S18.snp:0.14790)0.792:0.03904)0.565:0.01588)0.502:0.02553,((P4_11_S47.snp:0.16739,P2_2_S14.snp:0.20035)0.689:0.05770,P3_5_S29.snp:0.19236)0.353:0.02292)0.898:0.05067)0.396:0.01068,(((P5_11_S59.snp:0.19485,(P2_11_S23.snp:0.14303,(((((((((P1_11_S11.snp:0.10182,(ref:0.03288,(P2_3_S15.snp:0.04434,P3_9_S33.snp:0.07348)0.896:0.01630)0.749:0.00659)0.824:0.00949,(P2_4_S16.snp:0.35685,P1_10_S10.snp:0.11676)0.872:0.05183)0.774:0.00656,P1_1_S1.snp:0.07115)0.951:0.02362,P1_9_S9.snp:0.01904)0.746:0.00054,(P1_3_S3.snp:0.09654,P1_6_S6.snp:0.08619)0.921:0.02776)0.834:0.01612,(P1_5_S5.snp:0.11512,P1_4_S4.snp:0.08130)0.818:0.02022)0.730:0.01531,(P3_10_S34.snp:0.13829,(P1_12_S12.snp:0.19707,P2_12_S24.snp:0.13800)0.407:0.02772)0.466:0.02207)0.593:0.01685,((P2_7_S19.snp:0.05300,P1_7_S7.snp:0.17201)0.959:0.04524,(P4_1_S37.snp:0.14943,P1_8_S8.snp:0.11963)0.930:0.03818)0.342:0.00630)0.883:0.02607,P3_1_S25.snp:0.18891)0.755:0.04276)0.544:0.02407)0.625:0.02877,(((P4_9_S45.snp:0.14180,P2_10_S22.snp:0.21974)0.963:0.06408,P5_10_S58.snp:0.12125)0.156:0.03904,P5_9_S57.snp:0.13317)0.715:0.03843)0.897:0.03721,(P3_4_S28.snp:0.15609,((P3_8_S32.snp:0.12498,P2_8_S20.snp:0.16136)0.698:0.05588,(P2_1_S13.snp:0.18497,P1_2_S2.snp:0.18470)0.768:0.06659)0.627:0.02908)0.701:0.02879)0.909:0.02384,(P3_11_S35.snp:0.12671,P3_3_S27.snp:0.21281)0.956:0.04540)";

        //var ex = "(A:0.1,B:0.2,(C:0.3,D:0.4):0.5);"
        tree.load(treetxt);

        tree.setNodeSize(5);
        //this._tree.setSize(500, 1500);
        tree.setSize(400, 1500);
        

        tree.disableZoom= true;


        
        tree.setTreeType('rectangular'); // Supported for rectangular, circular, and hierarchical tree types
        tree.alignLabels = true; // false to reset
        tree.showLabels =true;
        tree.offsety =80;
        tree.offsetx =10;

        //this._tree.setRoot("P10U44-20degres-A1.snp");

        console.log( tree);


        tree.draw();
        this.computeleavesTab();   
        //console.log(this._dictStrain['P5_9_S57.snp']);

        this._pixelratio= this.getPixelRatio(this._tree.canvas);
        console.log(this._pixelratio);
        //this._offsetY= this._tree.offsety*pixelRatio; 
        this._offsetY= this._tree.offsety;
        console.log(this._tree.offsety);
        console.log(this._offsetY);
        
        var why = this;
        /*this._tree.on('click', function (e) {
          
          var node = why._tree.getNodeAtMousePosition(e);
          console.log(node);
          if (node) {
            why._tree.offsety =10;
            why._tree.redrawFromBranch(node);
            
          } else {
            why._tree.offsety =10;
            why._tree.redrawOriginalTree();
          }
        });*/
        /*this._tree.on('updated', ({ property, nodeIds }) => {
          if (property === 'selected') {
            console.log(nodeIds);
            
            this._tree.draw();

          }
        });*/
       
        /*for (const leaf of tree.leaves) {
          leaf.data = {
            temp: {
              colour: this.getRandomColor(),  
              label: leaf.id ,
            },
            cell: {
              colour: this.getRandomColor(),  
              
            },
          };
      
        }  */

       /* for (var i = 0; i < tree.leaves.length; i++) {
          tree.leaves[i].data = {
            column1: {
              colour: '#3C7383',
              label: 'Label' + (i + 1),
            },
            column2: '#9BB7BF',
            column3: '#3C7383',
            column4: '#9BB7BF',
          };
        }
      */
        why._tree.draw();

        this._root =  this._tree.root;
        this._tree.on('click', function (e) {
          
          console.log(why._manager._strainManager)
          console.log(tree);
          /* for (var i = 0; i < tree.leaves.length; i++) {
              tree.leaves[i].data = {
                column1: {
                  colour: why.getRandomColor(),
                  
                },
                column2: why.getRandomColor(),
                column3: why.getRandomColor(),
                column4: why.getRandomColor()
              };
            }*/
            var node = why._tree.getNodeAtMousePosition(e);
            console.log(node);
            if (node) {
              
              
            
            //  why._tree.redrawFromBranch(node);
              why._tree.setRoot(node);
              why._tree.draw();
              
            } else {
              //why._tree.setSize(400, 1500);
              
              
              why._tree.setRoot(why._root);
              why._tree.draw();
            }
        });        

    

	}
  getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
	computeLeafY(strain){
        //console.log(strain)
        //console.log(this._tree);
        //console.log(this._tree.leaves[strain]);

        var variable = this._dictStrain[strain];

        var y =  this._tree.leaves[variable].centery*this._tree.zoom/this._pixelratio+this._offsetY;
        return y;

    }
    getPixelRatio(canvas) {
        return (window.devicePixelRatio || 1) / this.getBackingStorePixelRatio(canvas);
    }
    
    getBackingStorePixelRatio(context) {
        return (
          context.backingStorePixelRatio ||
          context.webkitBackingStorePixelRatio ||
          context.mozBackingStorePixelRatio ||
          context.msBackingStorePixelRatio ||
          context.oBackingStorePixelRatio ||
          1
        );
      }
	
    computeleavesTab(){

        var dict = {};
        for (var variable in this._tree.leaves){
            //console.log(variable);
            //console.log(this._tree.leaves[variable].id);
            dict[this._tree.leaves[variable].id]=variable;
        }

        this._dictStrain = dict;
    }

}

export default TreeLoader;