class FastaLoader{

	constructor(file){
		this._file = file;
		this._header = "";
		this._seq = "";
	}
	

	async read(){
		console.log(this._file);
		this._seq = await this.load(this._file);
		console.log(this._seq);
		//console.log("read Fini" );


		var data = this._seq.split(/\n\r?/gi);

		
    	while (data.length && data[0][0] === '>') {
      		data.shift();
    	}

    	console.log(data.join(''));
    	this._seq = data.join('');
    	//console.log(this.getSubSeq(0,1));
    	//console.log(this.getSubSeq(0,15));
    	//console.log(this.getSubSeq(5533,5536));
    	//console.log("l "+this._seq.length);

	}

	/*loadOld(ressource) {
	    var xhr = new XMLHttpRequest();
	    await xhr.open("GET", ressource , false);
	    xhr.send(null);
	    return xhr.responseText;
	}*/
	

	/*load(url) {
	  return new Promise(function(resolve, reject) {
	    const xhr = new XMLHttpRequest();
	    xhr.onreadystatechange = function(e) {
	      if (xhr.readyState === 4) {
	        if (xhr.status === 200) {
	          resolve(xhr.response);
	        } else {
	          reject(xhr.status);
	        }
	      }
	    }
	    xhr.ontimeout = function () {
	      reject('timeout');
	    }
	    xhr.open('get', url, true);
	    xhr.send();

	    
	  })
	}*/

	async load(url) {

		var why =this;
		return new Promise(function(resolve, reject) {
		  const xhr = new XMLHttpRequest();
		  xhr.onreadystatechange = function(e) {
			if (xhr.readyState === 4) {
			  if (xhr.status === 200) {
				why._data = xhr.response;
				resolve(why);

			  } else {
				reject(xhr.status);
			  }
			}
		  }
		  xhr.ontimeout = function () {
			reject('timeout');
		  }
		  xhr.open('get', url, true);
		  xhr.send();

		  
	  });
  }


	getSubSeq(begin,end){
		let subSeq =this._seq.substring(begin, end);

		return subSeq;

	}

}

export default FastaLoader;