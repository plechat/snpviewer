# SNPViewer


http://hub18.hosting.pasteur.fr/SynTView/Data/snpViewerDemo.mp4

# Documentation

## 1. Pre-requisites

   * You have `npm` and `parcel` installed.
   ParcelJS: `npm install -g parcel-bundler`
   * 


## 2. Build and Publish

   * use [ParcelJS](https://parceljs.org/) as a building tools.
     `npm run build` will build snpviewer into `dist/`

   * or use parcel to run the project locally in http://localhost:8080/
     npm run start 

   * or test directly the gitlab pages in [snpviewer](https://plechat.pages.pasteur.fr/snpviewer/)

## 3. Data Preparation

   * Most files should be provided in a tabulated format.
### Annotation
   * SynTView can read feature information from NCBI ptt files. These protein table files can be obtained for bacteria genomes from NCBI's ftp site: ftp://ftp.ncbi.nih.gov/genomes/Bacteria. (or use a GenBank file and a wrapper script written in Bioperl as provided here)

   * Below the head of FN598874.ptt file :
```Helicobacter pylori B8, complete genome - 1..1673997
1711 proteins

Location	Strand	Length	PID	Gene	Synonym	Code	COG	Product
154..1527	+	457	298354686	dnaA	HPB8_1	-	-	chromosomal replication initiator protein DnaA

1756..3195	+	479	298354687	-	HPB8_2	-	-	conserved hypothetical protein

...
### SNP files


### Phylogenetic tree
The Newick format is described here The names of the leaf nodes are the name of the ptt file.
```((FQ670179:25.416891,(CP000153:24.935984,(CP000538:26.817907,(CP001816:24.827831,(CP000012:22.373966,(FN555004:21.37438,AE017125:21.72122):0.7671566):0.2101059):0.4582672):0.48131752):3.414669):9.554459,(((CP002336:2.316484,AM260522:3.6015763):2.2559552,(
CP002332:0.5595603,CP002184:1.7166162):0.14640999,AE001439:1.9021473):0.43751955):0.0,(CP001173:1.2607863,(CP000241:1.8142213,(((CP001680:0.49685884,(CP002096:1.3150878,((CP002076:0.18996191,(CP002071:0.055541277,CP001072:0.24794841):0.113527775):0.369269
,(CP001582:0.55765057,CP000012:0.50456715):0.16183925):0.16442347):0.62226176):0.43979788,(CP002331:1.5892277,CP002334:0.99043846):0.28099108):0.30312443,((FN598874:0.010840178,B128:0.14090848):1.3261669,((FM991728:1.107264,(CP002074:0.59512377,CP002073:0
92232466):0.33431387):0.14433646,(CP001217:0.89065623,AE000511:1.0820305):0.19708943):0.0):0.29496455):0.0):0.108477116):1.3428841):9.554459);
```
### MetaData

## 4. Data Access (how to  load the data)

   * Drag and drop the files in the drag and drop area.
   * or deploy syntviewjs in a server (use the dist directory contenti build with npm). Put all the data files in a `Datafoo` directory
     and use http://localhost/SynTView/?dataDir=Datafoo
